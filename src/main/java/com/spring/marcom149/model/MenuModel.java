package com.spring.marcom149.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_MENU")
public class MenuModel {

	private Integer idMenu;
	private String codeMenu;
	private String nameMenu;
	private String controllerMenu;
	private Integer isDeleteMenu;
	private String createdByMenu;
	private Date createdDateMenu;
	private String updatedByMenu;
	private Date updatedDateMenu;
	
	@Id
	@Column(name="ID_MENU")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="M_MENU")
	@TableGenerator(name="M_MENU",table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
	pkColumnValue="M_MENU_ID", valueColumnName="SEQUENCE_VALUE", allocationSize=1,initialValue=1)
	public Integer getIdMenu() {
		return idMenu;
	}
	public void setIdMenu(Integer idMenu) {
		this.idMenu = idMenu;
	}
	@Column(name="CODE_MENU")
	public String getCodeMenu() {
		return codeMenu;
	}
	public void setCodeMenu(String codeMenu) {
		this.codeMenu = codeMenu;
	}
	@Column(name="NAME_MENU")
	public String getNameMenu() {
		return nameMenu;
	}
	public void setNameMenu(String nameMenu) {
		this.nameMenu = nameMenu;
	}
	@Column(name="CONTROLLER_MENU")
	public String getControllerMenu() {
		return controllerMenu;
	}
	public void setControllerMenu(String controllerMenu) {
		this.controllerMenu = controllerMenu;
	}
	@Column(name="IS_DELETE_MENU")
	public Integer getIsDeleteMenu() {
		return isDeleteMenu;
	}
	public void setIsDeleteMenu(Integer isDeleteMenu) {
		this.isDeleteMenu = isDeleteMenu;
	}
	@Column(name="CREATED_BY_MENU")
	public String getCreatedByMenu() {
		return createdByMenu;
	}
	public void setCreatedByMenu(String createdByMenu) {
		this.createdByMenu = createdByMenu;
	}
	@Column(name="CREATED_DATE_MENU")
	public Date getCreatedDateMenu() {
		return createdDateMenu;
	}
	public void setCreatedDateMenu(Date createdDateMenu) {
		this.createdDateMenu = createdDateMenu;
	}
	@Column(name="UPDATED_BY_MENU")
	public String getUpdatedByMenu() {
		return updatedByMenu;
	}
	public void setUpdatedByMenu(String updatedByMenu) {
		this.updatedByMenu = updatedByMenu;
	}
	@Column(name="UPDATED_DATE_MENU")
	public Date getUpdatedDateMenu() {
		return updatedDateMenu;
	}
	public void setUpdatedDateMenu(Date updatedDateMenu) {
		this.updatedDateMenu = updatedDateMenu;
	}
	
}
