package com.spring.marcom149.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_MENU_ACCESS")
public class MenuAccessModel {

	private Integer idMenuAccess;
	private Integer idRole;
	private RoleModel roleModel;
	private Integer idMenu;
	private MenuModel menuModel;
	private Integer isDeleteMenuAccess;
	private String createdByMenuAccess;
	private Date createdDateMenuAccess;
	private String updatedByMenuAccess;
	private Date updatedDateMenuAccess;
	
	@Id
	@Column(name="ID_MENU_ACCESS")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="M_MENU_ACCESS")
	@TableGenerator(name="M_MENU_ACCESS",table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
	pkColumnValue="M_MENU_ACCESS_ID", valueColumnName="SEQUENCE_VALUE", allocationSize=1,initialValue=1)
	public Integer getIdMenuAccess() {
		return idMenuAccess;
	}
	public void setIdMenuAccess(Integer idMenuAccess) {
		this.idMenuAccess = idMenuAccess;
	}
	@Column(name="ID_ROLE")
	public Integer getIdRole() {
		return idRole;
	}
	public void setIdRole(Integer idRole) {
		this.idRole = idRole;
	}
	@ManyToOne
	@JoinColumn(name="ID_ROLE", nullable = false, updatable = false, insertable = false)
	public RoleModel getRoleModel() {
		return roleModel;
	}
	public void setRoleModel(RoleModel roleModel) {
		this.roleModel = roleModel;
	}
	@Column(name="ID_MENU")
	public Integer getIdMenu() {
		return idMenu;
	}
	public void setIdMenu(Integer idMenu) {
		this.idMenu = idMenu;
	}
	@ManyToOne
	@JoinColumn(name="ID_MENU", nullable = false, updatable = false, insertable = false)
	public MenuModel getMenuModel() {
		return menuModel;
	}
	public void setMenuModel(MenuModel menuModel) {
		this.menuModel = menuModel;
	}
	@Column(name="IS_DELETE_MENU_ACCESS")
	public Integer getIsDeleteMenuAccess() {
		return isDeleteMenuAccess;
	}
	public void setIsDeleteMenuAccess(Integer isDeleteMenuAccess) {
		this.isDeleteMenuAccess = isDeleteMenuAccess;
	}
	@Column(name="CREATED_BY_MENU_ACCESS")
	public String getCreatedByMenuAccess() {
		return createdByMenuAccess;
	}
	public void setCreatedByMenuAccess(String createdByMenuAccess) {
		this.createdByMenuAccess = createdByMenuAccess;
	}
	@Column(name="CREATED_DATE_MENU_ACCESS")
	public Date getCreatedDateMenuAccess() {
		return createdDateMenuAccess;
	}
	public void setCreatedDateMenuAccess(Date createdDateMenuAccess) {
		this.createdDateMenuAccess = createdDateMenuAccess;
	}
	@Column(name="UPDATED_BY_MENU_ACCESS")
	public String getUpdatedByMenuAccess() {
		return updatedByMenuAccess;
	}
	public void setUpdatedByMenuAccess(String updatedByMenuAccess) {
		this.updatedByMenuAccess = updatedByMenuAccess;
	}
	@Column(name="UPDATED_DATE_MENU_ACCESS")
	public Date getUpdatedDateMenuAccess() {
		return updatedDateMenuAccess;
	}
	public void setUpdatedDateMenuAccess(Date updatedDateMenuAccess) {
		this.updatedDateMenuAccess = updatedDateMenuAccess;
	}
	

}
