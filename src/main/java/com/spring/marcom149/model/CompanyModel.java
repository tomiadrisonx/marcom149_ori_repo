package com.spring.marcom149.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_COMPANY")
public class CompanyModel {
	
	private Integer idCompany;
	private String codeCompany;
	private String nameCompany;
	private String addressCompany;
	private String phoneCompany;
	private String emailCompany;
	
	private String createdByCompany;
	private Date createdDateCompany;
	private String updatedByCompany;
	private Date updatedDateCompany;
	private Integer isDeleteCompany;
	
	@Id
	@Column(name="ID_COMPANY")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="M_COMPANY")
	@TableGenerator(name="M_COMPANY", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
					pkColumnValue="M_COMPANY_ID", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getIdCompany() {
		return idCompany;
	}
	public void setIdCompany(Integer idCompany) {
		this.idCompany = idCompany;
	}
	@Column(name="CODE_COMPANY")
	public String getCodeCompany() {
		return codeCompany;
	}
	public void setCodeCompany(String codeCompany) {
		this.codeCompany = codeCompany;
	}
	@Column(name="NAME_COMPANY")
	public String getNameCompany() {
		return nameCompany;
	}
	public void setNameCompany(String nameCompany) {
		this.nameCompany = nameCompany;
	}
	@Column(name="ADDRESS_COMPANY")
	public String getAddressCompany() {
		return addressCompany;
	}
	public void setAddressCompany(String addressCompany) {
		this.addressCompany = addressCompany;
	}
	@Column(name="PHONE_COMPANY")
	public String getPhoneCompany() {
		return phoneCompany;
	}
	public void setPhoneCompany(String phoneCompany) {
		this.phoneCompany = phoneCompany;
	}
	@Column(name="EMAIL_COMPANY")
	public String getEmailCompany() {
		return emailCompany;
	}
	public void setEmailCompany(String emailCompany) {
		this.emailCompany = emailCompany;
	}
	@Column(name="CREATED_BY_COMPANY")
	public String getCreatedByCompany() {
		return createdByCompany;
	}
	public void setCreatedByCompany(String createdByCompany) {
		this.createdByCompany = createdByCompany;
	}
	@Column(name="CREATED_DATE_COMPANY")
	public Date getCreatedDateCompany() {
		return createdDateCompany;
	}
	public void setCreatedDateCompany(Date createdDateCompany) {
		this.createdDateCompany = createdDateCompany;
	}
	@Column(name="UPDATED_BY_COMPANY")
	public String getUpdatedByCompany() {
		return updatedByCompany;
	}
	public void setUpdatedByCompany(String updatedByCompany) {
		this.updatedByCompany = updatedByCompany;
	}
	@Column(name="UPDATED_DATE_COMPANY")
	public Date getUpdatedDateCompany() {
		return updatedDateCompany;
	}
	public void setUpdatedDateCompany(Date updatedDateCompany) {
		this.updatedDateCompany = updatedDateCompany;
	}
	@Column(name="IS_DELETE_COMPANY")
	public Integer getIsDeleteCompany() {
		return isDeleteCompany;
	}
	public void setIsDeleteCompany(Integer isDeleteCompany) {
		this.isDeleteCompany = isDeleteCompany;
	}
	
	
	
	

}
