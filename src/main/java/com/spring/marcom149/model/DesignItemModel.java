package com.spring.marcom149.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_DESIGN_ITEM")
public class DesignItemModel {

	private Integer idDesignItem;
	private Integer idDesign;
	private DesignModel designModel;
	private Integer idProduct;
	private ProductModel productModel;
	private String titleDesignItem;
	private Integer requestPicDesignItem;
	private EmployeeModel employeeModel;
	private Date startDateDesignItem;
	private Date endDateDesignItem;
	private Date requestDueDateDesignItem;
	private String noteDesignItem;
	private Integer isDeleteDesignItem;
	private String createdByDesignItem;
	private Date createdDateDesignItem;
	private String updatedByDesignItem;
	private Date updatedDateDesignItem;
	
	@Id
	@Column(name="ID_DESIGN_ITEM")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_DESIGN_ITEM")
	@TableGenerator(name="M_DESIGN_ITEM", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
					pkColumnValue="M_DESIGN_ITEM_ID", valueColumnName="SEQUENCE_VALUE",
					allocationSize=1, initialValue=1)
	public Integer getIdDesignItem() {
		return idDesignItem;
	}
	public void setIdDesignItem(Integer idDesignItem) {
		this.idDesignItem = idDesignItem;
	}
	@Column(name="ID_DESIGN")
	public Integer getIdDesign() {
		return idDesign;
	}
	public void setIdDesign(Integer idDesign) {
		this.idDesign = idDesign;
	}
	@ManyToOne
	@JoinColumn(name="ID_DESIGN", nullable = false, updatable = false, insertable = false)
	public DesignModel getDesignModel() {
		return designModel;
	}
	public void setDesignModel(DesignModel designModel) {
		this.designModel = designModel;
	}
	@Column(name="ID_PRODUCT")
	public Integer getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(Integer idProduct) {
		this.idProduct = idProduct;
	}
	@ManyToOne
	@JoinColumn(name="ID_PRODUCT", nullable = false, updatable = false, insertable = false)
	public ProductModel getProductModel() {
		return productModel;
	}
	public void setProductModel(ProductModel productModel) {
		this.productModel = productModel;
	}
	@Column(name="TITLE_DESIGN_ITEM")
	public String getTitleDesignItem() {
		return titleDesignItem;
	}
	public void setTitleDesignItem(String titleDesignItem) {
		this.titleDesignItem = titleDesignItem;
	}
	@Column(name="REQUEST_PIC_DESIGN_ITEM")
	public Integer getRequestPicDesignItem() {
		return requestPicDesignItem;
	}
	public void setRequestPicDesignItem(Integer requestPicDesignItem) {
		this.requestPicDesignItem = requestPicDesignItem;
	}
	@ManyToOne
	@JoinColumn(name="ID_EMPLOYEE", nullable = false, updatable = false, insertable = false)
	public EmployeeModel getEmployeeModel() {
		return employeeModel;
	}
	public void setEmployeeModel(EmployeeModel employeeModel) {
		this.employeeModel = employeeModel;
	}
	@Column(name="START_DATE_DESIGN_ITEM")
	public Date getStartDateDesignItem() {
		return startDateDesignItem;
	}
	public void setStartDateDesignItem(Date startDateDesignItem) {
		this.startDateDesignItem = startDateDesignItem;
	}
	@Column(name="END_DATE_DESIGN_ITEM")
	public Date getEndDateDesignItem() {
		return endDateDesignItem;
	}
	public void setEndDateDesignItem(Date endDateDesignItem) {
		this.endDateDesignItem = endDateDesignItem;
	}
	@Column(name="REQUEST_DUE_DATE_DESIGN_ITEM")
	public Date getRequestDueDateDesignItem() {
		return requestDueDateDesignItem;
	}
	public void setRequestDueDateDesignItem(Date requestDueDateDesignItem) {
		this.requestDueDateDesignItem = requestDueDateDesignItem;
	}
	@Column(name="NOTE_DESIGN_ITEM")
	public String getNoteDesignItem() {
		return noteDesignItem;
	}
	public void setNoteDesignItem(String noteDesignItem) {
		this.noteDesignItem = noteDesignItem;
	}
	@Column(name="IS_DELETE_DESIGN_ITEM")
	public Integer getIsDeleteDesignItem() {
		return isDeleteDesignItem;
	}
	public void setIsDeleteDesignItem(Integer isDeleteDesignItem) {
		this.isDeleteDesignItem = isDeleteDesignItem;
	}
	@Column(name="CREATED_BY_DESIGN_ITEM")
	public String getCreatedByDesignItem() {
		return createdByDesignItem;
	}
	public void setCreatedByDesignItem(String createdByDesignItem) {
		this.createdByDesignItem = createdByDesignItem;
	}
	@Column(name="CREATED_DATE_DESIGN_ITEM")
	public Date getCreatedDateDesignItem() {
		return createdDateDesignItem;
	}
	public void setCreatedDateDesignItem(Date createdDateDesignItem) {
		this.createdDateDesignItem = createdDateDesignItem;
	}
	@Column(name="UPDATED_BY_DESIGN_ITEM")
	public String getUpdatedByDesignItem() {
		return updatedByDesignItem;
	}
	public void setUpdatedByDesignItem(String updatedByDesignItem) {
		this.updatedByDesignItem = updatedByDesignItem;
	}
	@Column(name="UPDATED_DATE_DESIGN_ITEM")
	public Date getUpdatedDateDesignItem() {
		return updatedDateDesignItem;
	}
	public void setUpdatedDateDesignItem(Date updatedDateDesignItem) {
		this.updatedDateDesignItem = updatedDateDesignItem;
	}
	
	
}
