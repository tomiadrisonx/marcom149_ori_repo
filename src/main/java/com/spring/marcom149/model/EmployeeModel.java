package com.spring.marcom149.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_EMPLOYEE")
public class EmployeeModel {
	
	
	private Integer idEmployee;
	private String codeEmployee;
	private String firstnameEmployee;
	private String lastnameEmployee;
	
	private Integer idCompany;
	private CompanyModel companyModel;
	
	private Integer idDivision;
	private DivisionModel divisionModel;
	
	private String emailEmployee;
	
	private String createdByEmployee;
	private Date createdDateEmployee;
	private String updatedByEmployee;
	private Date updatedDateEmployee;
	private Integer isDeleteEmployee;

	@Id
	@Column(name="ID_EMPLOYEE")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="M_EMPLOYEE")
	@TableGenerator(name="M_EMPLOYEE", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
					pkColumnValue="M_EMPLOYEE_ID", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getIdEmployee() {
		return idEmployee;
	}
	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}
	
	@Column(name="CODE_EMPLOYEE")
	public String getCodeEmployee() {
		return codeEmployee;
	}
	public void setCodeEmployee(String codeEmployee) {
		this.codeEmployee = codeEmployee;
	}
	
	@Column(name="FIRSTNAME_EMPLOYEE")
	public String getFirstnameEmployee() {
		return firstnameEmployee;
	}

	public void setFirstnameEmployee(String firstnameEmployee) {
		this.firstnameEmployee = firstnameEmployee;
	}
	
	@Column(name="LASTNAME_EMPLOYEE")
	public String getLastnameEmployee() {
		return lastnameEmployee;
	}
	public void setLastnameEmployee(String lastnameEmployee) {
		this.lastnameEmployee = lastnameEmployee;
	}
	
	@Column(name="ID_COMPANY")
	public Integer getIdCompany() {
		return idCompany;
	}
	public void setIdCompany(Integer idCompany) {
		this.idCompany = idCompany;
	}
	
	@ManyToOne
	@JoinColumn(name = "ID_COMPANY", nullable = false, updatable = false, insertable = false )
	public CompanyModel getCompanyModel() {
		return companyModel;
	}

	public void setCompanyModel(CompanyModel companyModel) {
		this.companyModel = companyModel;
	}
	@Column(name="EMAIL_EMPLOYEE")
	public String getEmailEmployee() {
		return emailEmployee;
	}

	public void setEmailEmployee(String emailEmployee) {
		this.emailEmployee = emailEmployee;
	}
	@Column(name="ID_DIVISION")
	public Integer getIdDivision() {
		return idDivision;
	}
	public void setIdDivision(Integer idDivision) {
		this.idDivision = idDivision;
	}
	@ManyToOne
	@JoinColumn(name = "ID_DIVISION", nullable = false, updatable = false, insertable = false )
	public DivisionModel getDivisionModel() {
		return divisionModel;
	}
	public void setDivisionModel(DivisionModel divisionModel) {
		this.divisionModel = divisionModel;
	}
	@Column(name="CREATED_BY_EMPLOYEE")
	public String getCreatedByEmployee() {
		return createdByEmployee;
	}
	public void setCreatedByEmployee(String createdByEmployee) {
		this.createdByEmployee = createdByEmployee;
	}
	@Column(name="CREATED_DATE_EMPLOYEE")
	public Date getCreatedDateEmployee() {
		return createdDateEmployee;
	}
	public void setCreatedDateEmployee(Date createdDateEmployee) {
		this.createdDateEmployee = createdDateEmployee;
	}
	@Column(name="UPDATED_BY_EMPLOYEE")
	public String getUpdatedByEmployee() {
		return updatedByEmployee;
	}
	public void setUpdatedByEmployee(String updatedByEmployee) {
		this.updatedByEmployee = updatedByEmployee;
	}
	@Column(name="UPDATED_DATE_EMPLOYEE")
	public Date getUpdatedDateEmployee() {
		return updatedDateEmployee;
	}
	public void setUpdatedDateEmployee(Date updatedDateEmployee) {
		this.updatedDateEmployee = updatedDateEmployee;
	}
	@Column(name="IS_DELETE_EMPLOYEE")
	public Integer getIsDeleteEmployee() {
		return isDeleteEmployee;
	}
	public void setIsDeleteEmployee(Integer isDeleteEmployee) {
		this.isDeleteEmployee = isDeleteEmployee;
	}
	
	
	
}
