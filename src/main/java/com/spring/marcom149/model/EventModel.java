package com.spring.marcom149.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;


@Entity
@Table(name="T_EVENT")
public class EventModel {

	private Integer idEvent;
	private String codeEvent;
	private String nameEvent;
	private Date startDateEvent;
	private Date endDateEvent;
	private String placeEvent;
	private Long budgetEvent;
	
	private Integer requestByEvent;
	private UserModel requestUserModel;
	private Date requestDateEvent;
	
	private Integer approvedByEvent;
	private UserModel approvedUserModel;
	private Date approvedDateEvent;
	
	private Integer assignToEvent;
	private UserModel assignUserModel;
	
	private Integer closeByEvent;
	private UserModel closeUserModel;
	private Date closedDateEvent;
	
	private String noteEvent;
	private Integer statusEvent;
	
	private Integer rejectByEvent;
	private UserModel rejectUserModel;
	private Date rejectDateEvent;
	
	private String rejectReasonEvent;
	
	private String createdByEvent;
	private Date createdDateEvent;
	private String updatedByEvent;
	private Date updatedDateEvent;
	private Integer isDeleteEvent;
	
	@Id
	@Column(name="ID_EVENT")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_EVENT")
	@TableGenerator(name="T_EVENT", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
					pkColumnValue="T_EVENT_ID", valueColumnName="SEQUENCE_VALUE",
					allocationSize=1, initialValue=1)
	public Integer getIdEvent() {
		return idEvent;
	}
	public void setIdEvent(Integer idEvent) {
		this.idEvent = idEvent;
	}
	
	@Column(name="CODE_EVENT")
	public String getCodeEvent() {
		return codeEvent;
	}
	public void setCodeEvent(String codeEvent) {
		this.codeEvent = codeEvent;
	}
	
	@Column(name="NAME_EVENT")
	public String getNameEvent() {
		return nameEvent;
	}
	public void setNameEvent(String nameEvent) {
		this.nameEvent = nameEvent;
	}
	
	@Column(name="START_DATE_EVENT")
	public Date getStartDateEvent() {
		return startDateEvent;
	}
	public void setStartDateEvent(Date startDateEvent) {
		this.startDateEvent = startDateEvent;
	}
	
	@Column(name="END_DATE_EVENT")
	public Date getEndDateEvent() {
		return endDateEvent;
	}
	public void setEndDateEvent(Date endDateEvent) {
		this.endDateEvent = endDateEvent;
	}
	
	@Column(name="PLACE_EVENT")
	public String getPlaceEvent() {
		return placeEvent;
	}
	public void setPlaceEvent(String placeEvent) {
		this.placeEvent = placeEvent;
	}
	
	@Column(name="BUDGET_EVENT")
	public Long getBudgetEvent() {
		return budgetEvent;
	}
	public void setBudgetEvent(Long budgetEvent) {
		this.budgetEvent = budgetEvent;
	}
	
	@Column(name="REQUEST_BY_EVENT")
	public Integer getRequestByEvent() {
		return requestByEvent;
	}
	public void setRequestByEvent(Integer requestByEvent) {
		this.requestByEvent = requestByEvent;
	}
	
	@ManyToOne
	@JoinColumn(name = "REQUEST_BY_EVENT", nullable=true, updatable=false, insertable=false)
	public UserModel getRequestUserModel() {
		return requestUserModel;
	}
	public void setRequestUserModel(UserModel requestUserModel) {
		this.requestUserModel = requestUserModel;
	}
	
	@Column(name="REQUEST_DATE_EVENT")
	public Date getRequestDateEvent() {
		return requestDateEvent;
	}
	public void setRequestDateEvent(Date requestDateEvent) {
		this.requestDateEvent = requestDateEvent;
	}
	
	@Column(name="APPROVED_BY_EVENT")
	public Integer getApprovedByEvent() {
		return approvedByEvent;
	}
	public void setApprovedByEvent(Integer approvedByEvent) {
		this.approvedByEvent = approvedByEvent;
	}
	
	@ManyToOne
	@JoinColumn(name = "APPROVED_BY_EVENT", nullable=true, updatable=false, insertable=false)
	
	public UserModel getApprovedUserModel() {
		return approvedUserModel;
	}
	public void setApprovedUserModel(UserModel approvedUserModel) {
		this.approvedUserModel = approvedUserModel;
	}
	
	@Column(name="APPROVED_DATE_EVENT")
	public Date getApprovedDateEvent() {
		return approvedDateEvent;
	}
	public void setApprovedDateEvent(Date approvedDateEvent) {
		this.approvedDateEvent = approvedDateEvent;
	}
	
	@Column(name="ASSIGN_TO_EVENT")
	public Integer getAssignToEvent() {
		return assignToEvent;
	}
	public void setAssignToEvent(Integer assignToEvent) {
		this.assignToEvent = assignToEvent;
	}
	
	@ManyToOne
	@JoinColumn(name = "ASSIGN_TO_EVENT", nullable=true, updatable=false, insertable=false)
	public UserModel getAssignUserModel() {
		return assignUserModel;
	}
	public void setAssignUserModel(UserModel assignUserModel) {
		this.assignUserModel = assignUserModel;
	}
	
	@Column(name="CLOSED_DATE_EVENT")
	public Date getClosedDateEvent() {
		return closedDateEvent;
	}
	public void setClosedDateEvent(Date closedDateEvent) {
		this.closedDateEvent = closedDateEvent;
	}
	
	@Column(name="NOTE_EVENT")
	public String getNoteEvent() {
		return noteEvent;
	}
	public void setNoteEvent(String noteEvent) {
		this.noteEvent = noteEvent;
	}
	
	@Column(name="STATUS_EVENT")
	public Integer getStatusEvent() {
		return statusEvent;
	}
	public void setStatusEvent(Integer statusEvent) {
		this.statusEvent = statusEvent;
	}
	
	@Column(name="REJECT_REASON_EVENT")
	public String getRejectReasonEvent() {
		return rejectReasonEvent;
	}
	public void setRejectReasonEvent(String rejectReasonEvent) {
		this.rejectReasonEvent = rejectReasonEvent;
	}
	
	@Column(name="CREATED_BY_EVENT")
	public String getCreatedByEvent() {
		return createdByEvent;
	}
	public void setCreatedByEvent(String createdByEvent) {
		this.createdByEvent = createdByEvent;
	}
	
	@Column(name="CREATED_DATE_EVENT")
	public Date getCreatedDateEvent() {
		return createdDateEvent;
	}
	public void setCreatedDateEvent(Date createdDateEvent) {
		this.createdDateEvent = createdDateEvent;
	}
	
	@Column(name="UPDATED_BY_EVENT")
	public String getUpdatedByEvent() {
		return updatedByEvent;
	}
	public void setUpdatedByEvent(String updatedByEvent) {
		this.updatedByEvent = updatedByEvent;
	}
	
	@Column(name="UPDATED_DATE_EVENT")
	public Date getUpdatedDateEvent() {
		return updatedDateEvent;
	}
	public void setUpdatedDateEvent(Date updatedDateEvent) {
		this.updatedDateEvent = updatedDateEvent;
	}
	
	@Column(name="IS_DELETE_EVENT")
	public Integer getIsDeleteEvent() {
		return isDeleteEvent;
	}
	public void setIsDeleteEvent(Integer isDeleteEvent) {
		this.isDeleteEvent = isDeleteEvent;
	}
	@Column(name="CLOSE_BY_EVENT")
	public Integer getCloseByEvent() {
		return closeByEvent;
	}
	public void setCloseByEvent(Integer closeByEvent) {
		this.closeByEvent = closeByEvent;
	}
	@ManyToOne
	@JoinColumn(name = "CLOSE_BY_EVENT", nullable=true, updatable=false, insertable=false)
	public UserModel getCloseUserModel() {
		return closeUserModel;
	}
	public void setCloseUserModel(UserModel closeUserModel) {
		this.closeUserModel = closeUserModel;
	}
	@Column(name="REJECT_BY_EVENT")
	public Integer getRejectByEvent() {
		return rejectByEvent;
	}
	public void setRejectByEvent(Integer rejectByEvent) {
		this.rejectByEvent = rejectByEvent;
	}
	@ManyToOne
	@JoinColumn(name = "REJECT_BY_EVENT", nullable=true, updatable=false, insertable=false)
	public UserModel getRejectUserModel() {
		return rejectUserModel;
	}
	public void setRejectUserModel(UserModel rejectUserModel) {
		this.rejectUserModel = rejectUserModel;
	}
	@Column(name="REJECT_DATE_EVENT")
	public Date getRejectDateEvent() {
		return rejectDateEvent;
	}
	public void setRejectDateEvent(Date rejectDateEvent) {
		this.rejectDateEvent = rejectDateEvent;
	}
	
	
}