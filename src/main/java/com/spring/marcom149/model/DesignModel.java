package com.spring.marcom149.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_DESIGN")
public class DesignModel {

	private Integer idDesign;
	private String codeDesign;
	
	//join t_event
	private Integer idEvent;
	private EventModel eventModel;
	
	
	private String titleHeaderDesign;
	//join 
	private Integer requestByDesign;
	private EmployeeModel requestEmployeeModel;
	
	private Date requestDateDesign;
	
	private Integer approvedByDesign;
	private EmployeeModel approvedEmployeeModel;
	
	private Date approvedDateDesign;
	
	private Integer assignToDesign;
	private EmployeeModel assignEmployeeModel;
	
	private Date closeDateDesign;
	private String noteDesign;
	private Integer statusDesign;
	private String rejectReasonDesign;
	
	private String createdByDesign;
	private Date createdDateDesign;
	private String updatedByDesign;
	private Date updatedDateDesign;
	private Integer isDeleteDesign;
	
	
	@Id
	@Column(name="ID_DESIGN")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="T_DESIGN")
	@TableGenerator(name="T_DESIGN", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
					pkColumnValue="T_DESIGN_ID", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getIdDesign() {
		return idDesign;
	}
	public void setIdDesign(Integer idDesign) {
		this.idDesign = idDesign;
	}
	
	@Column(name="CODE_DESIGN")
	public String getCodeDesign() {
		return codeDesign;
	}
	public void setCodeDesign(String codeDesign) {
		this.codeDesign = codeDesign;
	}
	
	@Column(name="ID_EVENT")	
	public Integer getIdEvent() {
		return idEvent;
	}
	public void setIdEvent(Integer idEvent) {
		this.idEvent = idEvent;
	}
	
	@ManyToOne
	@JoinColumn(name = "ID_EVENT", nullable = false, updatable = false, insertable = false )
	public EventModel getEventModel() {
		return eventModel;
	}
	public void setEventModel(EventModel eventModel) {
		this.eventModel = eventModel;
	}
	@Column(name="TITLE_HEADER_DESIGN")
	public String getTitleHeaderDesign() {
		return titleHeaderDesign;
	}
	public void setTitleHeaderDesign(String titleHeaderDesign) {
		this.titleHeaderDesign = titleHeaderDesign;
	}
	
	@Column(name="REQUEST_BY_DESIGN")
	public Integer getRequestByDesign() {
		return requestByDesign;
	}
	public void setRequestByDesign(Integer requestByDesign) {
		this.requestByDesign = requestByDesign;
	}
	
	@ManyToOne
	@JoinColumn(name = "ID_EMPLOYEE", nullable = false, updatable = false, insertable = false )
	public EmployeeModel getRequestEmployeeModel() {
		return requestEmployeeModel;
	}
	public void setRequestEmployeeModel(EmployeeModel requestEmployeeModel) {
		this.requestEmployeeModel = requestEmployeeModel;
	}	
	
	@Column(name="REQUEST_DATE_DESIGN")
	public Date getRequestDateDesign() {
		return requestDateDesign;
	}
	public void setRequestDateDesign(Date requestDateDesign) {
		this.requestDateDesign = requestDateDesign;
	}
	
	@Column(name="APPROVED_BY_DESIGN")
	public Integer getApprovedByDesign() {
		return approvedByDesign;
	}
	public void setApprovedByDesign(Integer approvedByDesign) {
		this.approvedByDesign = approvedByDesign;
	}
	
	@ManyToOne
	@JoinColumn(name = "ID_EMPLOYEE", nullable = false, updatable = false, insertable = false )
	public EmployeeModel getApprovedEmployeeModel() {
		return approvedEmployeeModel;
	}
	public void setApprovedEmployeeModel(EmployeeModel approvedEmployeeModel) {
		this.approvedEmployeeModel = approvedEmployeeModel;
	}
	
	@Column(name="APPROVED_DATE_DESIGN")
	public Date getApprovedDateDesign() {
		return approvedDateDesign;
	}
	public void setApprovedDateDesign(Date approvedDateDesign) {
		this.approvedDateDesign = approvedDateDesign;
	}
	
	@Column(name="ASSIGN_TO_DESIGN")
	public Integer getAssignToDesign() {
		return assignToDesign;
	}
	public void setAssignToDesign(Integer assignToDesign) {
		this.assignToDesign = assignToDesign;
	}
	
	@ManyToOne
	@JoinColumn(name = "ID_EMPLOYEE", nullable = false, updatable = false, insertable = false )
	public EmployeeModel getAssignEmployeeModel() {
		return assignEmployeeModel;
	}
	public void setAssignEmployeeModel(EmployeeModel assignEmployeeModel) {
		this.assignEmployeeModel = assignEmployeeModel;
	}
	
	@Column(name="CLOSED_DATE_DESIGN")
	public Date getCloseDateDesign() {
		return closeDateDesign;
	}
	public void setCloseDateDesign(Date closeDateDesign) {
		this.closeDateDesign = closeDateDesign;
	}
	
	@Column(name="NOTE_DESIGN")
	public String getNoteDesign() {
		return noteDesign;
	}
	public void setNoteDesign(String noteDesign) {
		this.noteDesign = noteDesign;
	}
	
	@Column(name="STATUS_DESIGN")
	public Integer getStatusDesign() {
		return statusDesign;
	}
	public void setStatusDesign(Integer statusDesign) {
		this.statusDesign = statusDesign;
	}
	
	@Column(name="REJECT_REASON_DESIGN")
	public String getRejectReasonDesign() {
		return rejectReasonDesign;
	}
	public void setRejectReasonDesign(String rejectReasonDesign) {
		this.rejectReasonDesign = rejectReasonDesign;
	}
	
	@Column(name="CREATED_BY_DESIGN")
	public String getCreatedByDesign() {
		return createdByDesign;
	}
	public void setCreatedByDesign(String createdByDesign) {
		this.createdByDesign = createdByDesign;
	}
	
	@Column(name="CREATED_DATE_DESIGN")
	public Date getCreatedDateDesign() {
		return createdDateDesign;
	}
	public void setCreatedDateDesign(Date createdDateDesign) {
		this.createdDateDesign = createdDateDesign;
	}
	
	@Column(name="UPDATED_BY_DESIGN")
	public String getUpdatedByDesign() {
		return updatedByDesign;
	}
	public void setUpdatedByDesign(String updatedByDesign) {
		this.updatedByDesign = updatedByDesign;
	}
	
	@Column(name="UPDATED_DATE_DESIGN")
	public Date getUpdatedDateDesign() {
		return updatedDateDesign;
	}
	public void setUpdatedDateDesign(Date updatedDateDesign) {
		this.updatedDateDesign = updatedDateDesign;
	}
	
	@Column(name="IS_DELETE_DESIGN")
	public Integer getIsDeleteDesign() {
		return isDeleteDesign;
	}
	public void setIsDeleteDesign(Integer isDeleteDesign) {
		this.isDeleteDesign = isDeleteDesign;
	}
	
}
