package com.spring.marcom149.dao;

import java.util.List;

import com.spring.marcom149.model.DesignItemModel;

public interface DesignItemDao {

	public void create(DesignItemModel designItemModel) throws Exception;
	
	public void update(DesignItemModel designItemModel) throws Exception;
	
	public void delete(DesignItemModel designItemModel) throws Exception;
	
	public  List<DesignItemModel> list() throws Exception;
	
	public DesignItemModel searchById(Integer idDesignItem) throws Exception;
	
}
