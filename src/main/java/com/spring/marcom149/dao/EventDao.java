package com.spring.marcom149.dao;

import java.util.List;

import com.spring.marcom149.model.EventModel;

public interface EventDao {
	
	/*transaksi create*/
	public void create(EventModel eventModel) throws Exception;
	
	/*transaksi update*/
	public void update(EventModel eventModel) throws Exception;
	
	/*transaksi delete*/
	public void delete(EventModel eventModel) throws Exception;
	
	/*transaksi search*/
	public List<EventModel> list() throws Exception;
	
	/*transaksi searchByKode*/
	public EventModel searchByKode(String codeEvent) throws Exception;
	
	/*transaksi searchById*/
	public EventModel searchById(Integer idEvent) throws Exception;
	
	public List<EventModel> listByRequester(Integer idUser) throws Exception;
	
	public List<EventModel> listByAssignTo(Integer idUser) throws Exception;
	
	public List<EventModel> listByApprove() throws Exception;

}
