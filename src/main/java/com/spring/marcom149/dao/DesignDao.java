package com.spring.marcom149.dao;

import java.util.List;

import com.spring.marcom149.model.DesignModel;

public interface DesignDao {
	
	public void create(DesignModel designModel) throws Exception;
	public void update(DesignModel designModel) throws Exception;
	public void delete(DesignModel designModel) throws Exception;
	public List<DesignModel> list() throws Exception;
	
	/* transaksi searchByCode */
	public DesignModel searchByCode(String codeDesign) throws Exception;

	/* transaksi searchById */
	public DesignModel searchById(Integer idDesign) throws Exception;


}
