package com.spring.marcom149.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.marcom149.dao.DesignItemDao;
import com.spring.marcom149.model.DesignItemModel;

@Repository
public class DesignItemDaoImpl implements DesignItemDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(DesignItemModel designItemModel) throws Exception {
		// TODO Auto-generated method stub
		// ini sintaks awal untuk query hibernate
		Session session = this.sessionFactory.getCurrentSession();
		// seesion.save adalah query insert into dengan hibernate
		session.save(designItemModel);
	}

	@Override
	public void update(DesignItemModel designItemModel) throws Exception {
		// TODO Auto-generated method stub
		// ini sintaks awal untuk query hibernate
		Session session = this.sessionFactory.getCurrentSession();
		// seesion.save adalah query insert into dengan hibernate
		session.update(designItemModel);
	}

	@Override
	public void delete(DesignItemModel designItemModel) throws Exception {
		// TODO Auto-generated method stub
		// ini sintaks awal untuk query hibernate
		Session session = this.sessionFactory.getCurrentSession();
		// seesion.save adalah query insert into dengan hibernate
		session.delete(designItemModel);
	}

	@Override
	public DesignItemModel searchById(Integer idDesignItem) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		DesignItemModel result = session.get(DesignItemModel.class, idDesignItem);
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DesignItemModel> list() throws Exception {
		// TODO Auto-generated method stub
		//adalah syntax awal untuk query hibernatenya
		Session session = this.sessionFactory.getCurrentSession()	;
		
		List<DesignItemModel> result = session.createQuery(" from DesignItemModel where isDeleteDesignItem = 0 ").list();

		return result;
	}

	
}
