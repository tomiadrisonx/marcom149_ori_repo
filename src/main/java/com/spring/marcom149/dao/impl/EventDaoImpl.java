package com.spring.marcom149.dao.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.marcom149.dao.EventDao;
import com.spring.marcom149.model.EventModel;

@SuppressWarnings("deprecation")
@Repository
public class EventDaoImpl implements EventDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(EventModel event) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		// session save = query insert into dgn hibernate
		session.save(event);

	}

	@Override
	public void update(EventModel event) throws Exception {
		// TODO Auto-generated method stub

		Session session = this.sessionFactory.getCurrentSession();

		// session save = query insert into dgn hibernate
		session.update(event);

	}

	@Override
	public void delete(EventModel event) throws Exception {
		// TODO Auto-generated method stub

		Session session = this.sessionFactory.getCurrentSession();

		// session save = query insert into dgn hibernate
		session.delete(event);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EventModel> list() throws Exception {
		// TODO Auto-generated method stub
		// syntax awal untuk query hibernate

		Session session = this.sessionFactory.getCurrentSession();
		List<EventModel> result = session.createQuery("from EventModel where isDeleteEvent = 0").list();

		return result;
	}

	@Override
	public EventModel searchByKode(String codeEvent) throws Exception {
		// TODO Auto-generated method stu
		Session session = this.sessionFactory.getCurrentSession();
		EventModel result = null;
		try {
			result = (EventModel) session.createQuery("from EventModel Where codeEvent ='" + codeEvent + "' ")
					.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public EventModel searchById(Integer idEvent) throws Exception {
		// TODO Auto-generated method stu
		Session session = this.sessionFactory.getCurrentSession();
		EventModel result = session.get(EventModel.class, idEvent);
		return result;
	}

	@SuppressWarnings({  "rawtypes", "unchecked" })
	@Override
	public List<EventModel> listByRequester(Integer idUser) throws Exception {
		// TODO Auto-generated method stub
		String sqlquery = " SELECT "
						+ "		TEVE.ID_EVENT,	"
						+ "		TEVE.APPROVED_BY_EVENT, " 
						+ " 	TEVE.APPROVED_DATE_EVENT,	"
						+ "		TEVE.ASSIGN_TO_EVENT,	"
						+ "		TEVE.BUDGET_EVENT, "
						+ "		TEVE.CLOSE_BY_EVENT,	"
						+ "		TEVE.CLOSED_DATE_EVENT,	"
						+ "		TEVE.CODE_EVENT,	"
						+ "		TEVE.CREATED_BY_EVENT,	" //
						+ "		TEVE.CREATED_DATE_EVENT,	"
						+ "		TEVE.END_DATE_EVENT,	"
						+ "		TEVE.IS_DELETE_EVENT,	"
						+ "		TEVE.NAME_EVENT,	"
						+ "		TEVE.NOTE_EVENT,	"
						+ "		TEVE.PLACE_EVENT,	"
						+ "		TEVE.REJECT_BY_EVENT,	"
						+ "		TEVE.REJECT_DATE_EVENT,	"
						+ "		TEVE.REJECT_REASON_EVENT,	"
						+ "		TEVE.REQUEST_BY_EVENT,	"
						+ "		TEVE.REQUEST_DATE_EVENT,	"
						+ "		TEVE.START_DATE_EVENT,	"
						+ "		TEVE.STATUS_EVENT,	"
						+ "		TEVE.UPDATED_BY_EVENT,	"
						+ "		TEVE.UPDATED_DATE_EVENT	"
						+ "	FROM T_EVENT TEVE "	
						+ "	JOIN M_USER MUSE "
						+ "		ON MUSE.ID_USER = TEVE.REQUEST_BY_EVENT "	
						+ "	WHERE MUSE.ID_USER = "+idUser+" " ;
		
		Session session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(sqlquery);
		query.addEntity(EventModel.class);
		List<EventModel> result = query.list();
		return result;
	}

	@SuppressWarnings({  "unchecked" })
	@Override
	public List<EventModel> listByAssignTo(Integer idUser) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<EventModel> result = null;
		try {
			result = session.createQuery("from EventModel where assignToEvent ='" + idUser + "' and statusEvent = 2 ").list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EventModel> listByApprove() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<EventModel> result = null;
		try {
			result = session.createQuery("from EventModel where statusEvent = 1 ").list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}
}
