package com.spring.marcom149.dao.impl;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.marcom149.dao.DesignDao;
import com.spring.marcom149.model.DesignModel;

@Repository
public class DesignDaoImpl implements DesignDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(DesignModel designModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(designModel);
		
	}

	@Override
	public void update(DesignModel designModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(designModel);
		
	}

	@Override
	public void delete(DesignModel designModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(designModel);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DesignModel> list() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<DesignModel> result = session.createQuery("from DesignModel").list();
		return result;
	}

	@Override
	public DesignModel searchByCode(String codeDesign) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		DesignModel result = null;
		try {
			result = (DesignModel) session.createQuery("from DesignModel where codeDesign = '"+codeDesign+"' ").getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public DesignModel searchById(Integer idDesign) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		DesignModel result = session.get(DesignModel.class, idDesign);
		return result;
	}

}
