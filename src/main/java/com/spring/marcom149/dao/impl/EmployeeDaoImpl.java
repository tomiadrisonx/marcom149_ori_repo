package com.spring.marcom149.dao.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.marcom149.dao.EmployeeDao;
import com.spring.marcom149.model.EmployeeModel;

@SuppressWarnings("deprecation")
@Repository
public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(EmployeeModel employeeModel) throws Exception {
		// TODO Auto-generated method stub
		// adalah syntax awal untuk query hibernatenya
		Session session = this.sessionFactory.getCurrentSession();

		// session.save adalah query into dengan hibernate
		session.save(employeeModel);
	}

	@Override
	public void update(EmployeeModel employeeModel) throws Exception {
		// TODO Auto-generated method stub
		// adalah syntax awal untuk query hibernatenya
		Session session = this.sessionFactory.getCurrentSession();

		// session.save adalah query into dengan hibernate
		session.update(employeeModel);
	}

	@Override
	public void delete(EmployeeModel employeeModel) throws Exception {
		// TODO Auto-generated method stub
		// adalah syntax awal untuk query hibernatenya
		Session session = this.sessionFactory.getCurrentSession();

		// session.save adalah query into dengan hibernate
		session.delete(employeeModel);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EmployeeModel> list() throws Exception {
		// TODO Auto-generated method stub
		// adalah syntax awal untuk query hibernatenya
		Session session = this.sessionFactory.getCurrentSession();

		List<EmployeeModel> result = session.createQuery(" from EmployeeModel where isDeleteEmployee = 0 ").list();

		return result;
	}

	@Override
	public EmployeeModel searchByKode(String codeEmployee) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		EmployeeModel result = null;
		try {
			result = (EmployeeModel) session.createQuery("from EmployeeModel Where codeEmployee = '"+codeEmployee+"' ").getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public EmployeeModel searchById(Integer idEmployee) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		EmployeeModel result = session.get(EmployeeModel.class, idEmployee);
		return result;
	}

	@SuppressWarnings({"rawtypes", "unchecked" })
	@Override
	public List<EmployeeModel> listByRoleStaff() throws Exception {
		// TODO Auto-generated method stub
		String sqlquery = " SELECT "
						+ "		MEMP.ID_EMPLOYEE,	"
						+ "		MEMP.CODE_EMPLOYEE, "
						+ "     MEMP.CREATED_BY_EMPLOYEE," 
						+ "     MEMP.CREATED_DATE_EMPLOYEE,"
						+ "     MEMP.EMAIL_EMPLOYEE,"
						+ "     MEMP.FIRSTNAME_EMPLOYEE,"
						+ "     MEMP.ID_COMPANY,"
						+ "     MEMP.ID_DIVISION,"
						+ "     MEMP.IS_DELETE_EMPLOYEE,"
						+ "     MEMP.LASTNAME_EMPLOYEE,"
						+ "     MEMP.UPDATED_BY_EMPLOYEE,"
						+ "     MEMP.UPDATED_DATE_EMPLOYEE "
						+ "	FROM M_EMPLOYEE MEMP "	
						+ "	JOIN M_USER MUSE "
						+ "		ON MUSE.ID_EMPLOYEE = MEMP.ID_EMPLOYEE "
						+ "	JOIN M_ROLE MROL "
						+ "		ON MROL.ID_ROLE = MUSE.ID_ROLE "
						+ "	WHERE MROL.NAME_ROLE ='ROLE_STAFF' " ;
		
		Session session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(sqlquery);
		query.addEntity(EmployeeModel.class);
		List<EmployeeModel> result = query.list();
		return result;
	}

}
