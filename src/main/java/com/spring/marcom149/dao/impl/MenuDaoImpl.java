package com.spring.marcom149.dao.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.marcom149.dao.MenuDao;
import com.spring.marcom149.model.MenuModel;

@SuppressWarnings("deprecation")
@Repository
public class MenuDaoImpl implements MenuDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(MenuModel menuModel) throws Exception {
		// TODO Auto-generated method stub
		//ini sintax awal untuk query hibernatenya
		Session session = this.sessionFactory.getCurrentSession();
		//query insert into dgn hibernate
		session.save(menuModel);
	}

	@Override
	public void update(MenuModel menuModel) throws Exception {
		// TODO Auto-generated method stub
		//ini sintax awal untuk query hibernatenya
		Session session = this.sessionFactory.getCurrentSession();
		//query insert into dgn hibernate
		session.update(menuModel);
	}

	@Override
	public void delete(MenuModel menuModel) throws Exception {
		// TODO Auto-generated method stub
		//ini sintax awal untuk query hibernatenya
		Session session = this.sessionFactory.getCurrentSession();
		//query insert into dgn hibernate
		session.delete(menuModel);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MenuModel> list() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MenuModel> result = session.createQuery("From MenuModel where isDeleteMenu = 0").list();
		return result;
	}

	@Override
	public MenuModel searchByCode(String codeMenu) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		MenuModel result = null;
		try {
			result =(MenuModel) session.createQuery(" from MenuModel Where codeMenu ='"+codeMenu+"' ").getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public MenuModel searchById(Integer idMenu) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		MenuModel result = session.get(MenuModel.class, idMenu);
		return result;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	@Override
	public List<MenuModel> getAllMenuTree() throws Exception {
		// TODO Auto-generated method stub
		String sqlquery = " SELECT "
				+ "		ID_MENU ID_MENU,	"
				+ "		SYS_CONNECT_BY_PATH(ID_MENU,'.') CODE_MENU, "
				+ " 	CONTROLLER_MENU CONTROLLER_MENU,	"
				+ "		CREATED_BY_MENU CREATED_BY_MENU,	"
				+ "		CREATED_DATE_MENU CREATED_DATE_MENU, "
				+ "		IS_DELETE_MENU IS_DELETE_MENU,	"
				+ "		NAME_MENU NAME_MENU,	"
				+ "		UPDATED_BY_MENU UPDATED_BY_MENU,	"
				+ "		UPDATED_DATE_MENU UPDATED_DATE_MENU	" //G PAKE KOMA
				+ "		FROM M_MENU "	;
		
		Session session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(sqlquery);
		query.addEntity(MenuModel.class);
		List<MenuModel> result = query.list();
		return result;
				
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	@Override
	public List<MenuModel> getAllMenuTreeByRole(Integer idRole) throws Exception {
		// TODO Auto-generated method stub
		String sqlquery = " SELECT "
				+ "		M.ID_MENU ID_MENU,	"
				+ "		M.CODE_MENU CODE_MENU, " // CONNECT BY DIGANTI HANYA JD CODE_MENU
				+ " 	M.CONTROLLER_MENU CONTROLLER_MENU,	"
				+ "		M.CREATED_BY_MENU CREATED_BY_MENU,	"
				+ "		M.CREATED_DATE_MENU CREATED_DATE_MENU, "
				+ "		M.IS_DELETE_MENU IS_DELETE_MENU,	"
				+ "		M.NAME_MENU NAME_MENU,	"
				+ "		M.UPDATED_BY_MENU UPDATED_BY_MENU,	"
				+ "		M.UPDATED_DATE_MENU UPDATED_DATE_MENU	" // GA PAKE KOMA
				+ "	FROM M_MENU M "	
				+ "	JOIN M_MENU_ACCESS MA "
				+ "		ON MA.ID_MENU = M.ID_MENU "	
				+ "	WHERE MA.ID_ROLE = "+idRole+" " ;
		
		Session session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(sqlquery);
		query.addEntity(MenuModel.class);
		List<MenuModel> result = query.list();
		return result;
	}

}
