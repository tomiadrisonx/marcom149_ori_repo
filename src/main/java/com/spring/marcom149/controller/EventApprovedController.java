package com.spring.marcom149.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.marcom149.model.EmployeeModel;
import com.spring.marcom149.model.EventModel;
import com.spring.marcom149.model.MenuModel;
import com.spring.marcom149.model.UserModel;
import com.spring.marcom149.service.EmployeeService;
import com.spring.marcom149.service.EventService;
import com.spring.marcom149.service.MenuService;
//import com.spring.marcom149.service.UserService;

@Controller
public class EventApprovedController extends BaseController {

	@Autowired
	private EventService eventService;

	@Autowired
	private MenuService menuService;
	
	@Autowired
	private EmployeeService employeeService;
		
	/*@Autowired
	private UserService userService;*/

	public void accesLogin(Model model) {
		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("nameRole", this.getUserModel().getRoleModel().getNameRole());

		// logic untuk list menu secara hirarki by role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().idRole);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
	}
	// akhir method untuk menampilakan menu by role login dan user login

	// url method event event
	@RequestMapping(value = "event_approved")
	public String event(Model model) {
		this.accesLogin(model);
		return "event_approved";

	}
	

	
	// url method save
	@RequestMapping(value = "event_approved/save")
	public String save(HttpServletRequest approved, Model model) throws Exception {
		Integer idEvent = null;
		if (approved.getParameter("idEvent") != null) {
			idEvent = Integer.valueOf(approved.getParameter("idEvent"));
		}

		/* yang kiri adalah variable yang kanan dari jsp name nya */
		String codeEvent = approved.getParameter("codeEvent");
		String nameEvent = approved.getParameter("nameEvent");
		String placeEvent = approved.getParameter("placeEvent");
		Integer requestByEvent = Integer.valueOf(approved.getParameter("requestByEvent"));
		String noteEvent = approved.getParameter("noteEvent");
		Long budgetEvent = Long.valueOf(approved.getParameter("budgetEvent"));
		Integer assignToEvent = Integer.valueOf(approved.getParameter("assignToEvent"));
		String rejectReasonEvent = approved.getParameter("rejectReasonEvent");
		

		// code date
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
		Date startDateEvent = formatDate.parse(approved.getParameter("startDateEvent"));
		Date endDateEvent = formatDate.parse(approved.getParameter("endDateEvent"));
		Date requestDateEvent = formatDate.parse(approved.getParameter("requestDateEvent"));
		
		EventModel eventModel = new EventModel();

		eventModel.setIdEvent(idEvent);
		eventModel.setCodeEvent(codeEvent);
		eventModel.setNameEvent(nameEvent);
		eventModel.setPlaceEvent(placeEvent);
		eventModel.setBudgetEvent(budgetEvent);
		eventModel.setAssignToEvent(assignToEvent);
		eventModel.setRequestByEvent(requestByEvent);
		eventModel.setNoteEvent(noteEvent);
		eventModel.setStartDateEvent(startDateEvent);
		eventModel.setEndDateEvent(endDateEvent);
		eventModel.setRejectReasonEvent(rejectReasonEvent);
		eventModel.setRequestDateEvent(requestDateEvent);

		String process = approved.getParameter("process");

		if (process.equals("approve")) {

			UserModel user = new UserModel();
			user = this.getUserModel();
			eventModel.setApprovedByEvent(user.getIdUser());
			eventModel.setApprovedDateEvent(new Date());
			eventModel.setStatusEvent(2); // status eventnya 2 karena inprogress
			
			eventModel.setUpdatedDateEvent(new Date());
			eventModel.setUpdatedDateEvent(new Date());
			eventModel.setIsDeleteEvent(0);
			
			EventModel kmDB = new EventModel();
			kmDB = this.eventService.searchById(idEvent);

			eventModel.setCreatedByEvent(kmDB.getCreatedByEvent());
			eventModel.setCreatedDateEvent(kmDB.getCreatedDateEvent());
			
			this.eventService.update(eventModel);

		} else if (process.equals("reject")) {

			UserModel user = new UserModel();
			user = this.getUserModel();
			
			eventModel.setRejectByEvent(user.getIdUser());
			eventModel.setRejectDateEvent(new Date());
			eventModel.setStatusEvent(0);  // status eventnya 0 karena rejected

			EventModel kmDB = new EventModel();
			kmDB = this.eventService.searchById(idEvent);

			eventModel.setCreatedByEvent(kmDB.getCreatedByEvent());
			eventModel.setCreatedDateEvent(kmDB.getCreatedDateEvent());

			eventModel.setUpdatedByEvent(user.getUsername());
			eventModel.setUpdatedDateEvent(new Date());
			eventModel.setIsDeleteEvent(0);
			
			this.eventService.update(eventModel);

		} else {

		}
		// ini syntax untuk kirim variable process ke jsp
		model.addAttribute("process", process);
		model.addAttribute("eventModel", eventModel);

		// return supplier maksudnya memanggil file supplier.jsp
		return "event_approved";
	}

	// url method list
	@RequestMapping(value = "event_approved/list")
	private String list(Model model) throws Exception {
		List<EventModel> eventModelList = new ArrayList<EventModel>();
		eventModelList = this.eventService.listByApprove();
		model.addAttribute("eventModelList", eventModelList);

		return "event_approved/list";
	}

	// url method approve
	@RequestMapping(value = "event_approved/approve")
	private String approve(Model model, HttpServletRequest approved) throws Exception {
		Integer idEvent = Integer.valueOf(approved.getParameter("idEvent"));

		EventModel eventModel = new EventModel();
		eventModel = this.eventService.searchById(idEvent);
		model.addAttribute("eventModel", eventModel);
		
		
		// ASSIGN TO
		List<EmployeeModel> employeeModelList = new ArrayList<EmployeeModel>();
		employeeModelList = this.employeeService.listByRoleStaff();
		model.addAttribute("employeeModelList",employeeModelList);
		return "event_approved/approve";
		
		/*List<UserModel> userModelList = new ArrayList<UserModel>();
		userModelList = this.userService.list();
		model.addAttribute("userModelList",userModelList);
		return "event_approved/approve";*/

	}
	
	
		


	// url method detail
	@RequestMapping(value = "event_approved/detail")
	private String detail(Model model, HttpServletRequest approved) throws Exception {
		Integer idEvent = Integer.valueOf(approved.getParameter("idEvent"));

		EventModel eventModel = new EventModel();
		eventModel = this.eventService.searchById(idEvent);

		model.addAttribute("eventModel", eventModel);
		return "event_approved/detail";

	}

}
