package com.spring.marcom149.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.marcom149.model.EmployeeModel;
import com.spring.marcom149.model.EventModel;
import com.spring.marcom149.model.MenuModel;
import com.spring.marcom149.model.UserModel;
import com.spring.marcom149.service.EmployeeService;
import com.spring.marcom149.service.EventService;
import com.spring.marcom149.service.MenuService;

@Controller
public class EventCloseController extends BaseController {

	@Autowired
	private EventService eventService;

	@Autowired
	private MenuService menuService;
	
	@Autowired
	private EmployeeService employeeService;

	public void accesLogin(Model model) {
		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("nameRole", this.getUserModel().getRoleModel().getNameRole());

		// logic untuk list menu secara hirarki by role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().idRole);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
	}
	// akhir method untuk menampilakan menu by role login dan user login

	// url method event event
	@RequestMapping(value = "event_close")
	public String event(Model model) {
		this.accesLogin(model);
		return "event_close";

	}
	

	
	// url method save
	@RequestMapping(value = "event_close/save")
	public String save(HttpServletRequest closed, Model model) throws Exception {
		Integer idEvent = null;
		if (closed.getParameter("idEvent") != null) {
			idEvent = Integer.valueOf(closed.getParameter("idEvent"));
		}

		/* yang kiri adalah variable yang kanan dari jsp name nya */
		String codeEvent = closed.getParameter("codeEvent");
		String nameEvent = closed.getParameter("nameEvent");
		String placeEvent = closed.getParameter("placeEvent");
		Integer requestByEvent = Integer.valueOf(closed.getParameter("requestByEvent"));
		String noteEvent = closed.getParameter("noteEvent");
		Long budgetEvent = Long.valueOf(closed.getParameter("budgetEvent"));
		Integer assignToEvent = Integer.valueOf(closed.getParameter("assignToEvent"));
		String rejectReasonEvent = closed.getParameter("rejectReasonEvent");
		

		// code date
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
		Date startDateEvent = formatDate.parse(closed.getParameter("startDateEvent"));
		Date endDateEvent = formatDate.parse(closed.getParameter("endDateEvent"));
		Date requestDateEvent = formatDate.parse(closed.getParameter("requestDateEvent"));
	
		EventModel eventModel = new EventModel();

		eventModel.setIdEvent(idEvent);
		eventModel.setCodeEvent(codeEvent);
		eventModel.setNameEvent(nameEvent);
		eventModel.setPlaceEvent(placeEvent);
		eventModel.setBudgetEvent(budgetEvent);
		eventModel.setAssignToEvent(assignToEvent);
		eventModel.setRequestByEvent(requestByEvent);
		eventModel.setNoteEvent(noteEvent);
		eventModel.setStartDateEvent(startDateEvent);
		eventModel.setEndDateEvent(endDateEvent);
		eventModel.setRejectReasonEvent(rejectReasonEvent);
		eventModel.setRequestDateEvent(requestDateEvent);

		String process = closed.getParameter("process");

		if (process.equals("close")) {

			UserModel user = new UserModel();
			user = this.getUserModel();
			eventModel.setCloseByEvent(user.getIdUser());
			eventModel.setClosedDateEvent(new Date());
			eventModel.setIsDeleteEvent(0);
			eventModel.setStatusEvent(3); // status eventnya 3 karena close\
			
			EventModel kmDB = new EventModel();
			kmDB = this.eventService.searchById(idEvent);

			eventModel.setCreatedByEvent(kmDB.getCreatedByEvent());
			eventModel.setCreatedDateEvent(kmDB.getCreatedDateEvent());
			
			eventModel.setUpdatedDateEvent(new Date());
			eventModel.setUpdatedDateEvent(new Date());
			this.eventService.update(eventModel);
			
			

		} else {

		}
		// ini syntax untuk kirim variable process ke jsp
		model.addAttribute("process", process);
		model.addAttribute("eventModel", eventModel);

		// return supplier maksudnya memanggil file supplier.jsp
		return "event_close";
	}

	// url method list
	@RequestMapping(value = "event_close/list")
	private String list(Model model) throws Exception {
		List<EventModel> eventModelList = new ArrayList<EventModel>();
		
		
		eventModelList = this.eventService.listByAssignTo(this.getUserModel().getIdEmployee());
		model.addAttribute("eventModelList", eventModelList);

		return "event_close/list";
	}

	// url method approve
	@RequestMapping(value = "event_close/close")
	private String approve(Model model, HttpServletRequest approved) throws Exception {
		Integer idEvent = Integer.valueOf(approved.getParameter("idEvent"));

		EventModel eventModel = new EventModel();
		eventModel = this.eventService.searchById(idEvent);
		model.addAttribute("eventModel", eventModel);
		
		
		// ASSIGN TO
		List<EmployeeModel> employeeModelList = new ArrayList<EmployeeModel>();
		employeeModelList = this.employeeService.list();
		model.addAttribute("employeeModelList",employeeModelList);
		return "event_close/close";

	}
	
	
		


	// url method detail
	@RequestMapping(value = "event_close/detail")
	private String detail(Model model, HttpServletRequest approved) throws Exception {
		Integer idEvent = Integer.valueOf(approved.getParameter("idEvent"));

		EventModel eventModel = new EventModel();
		eventModel = this.eventService.searchById(idEvent);

		model.addAttribute("eventModel", eventModel);
		return "event_close/detail";

	}

}
