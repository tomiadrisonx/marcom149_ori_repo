package com.spring.marcom149.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.marcom149.model.MenuAccessModel;
import com.spring.marcom149.model.MenuModel;
import com.spring.marcom149.model.RoleModel;
import com.spring.marcom149.model.UserModel;
import com.spring.marcom149.service.MenuAccessService;
import com.spring.marcom149.service.MenuService;
import com.spring.marcom149.service.RoleService;

@Controller
public class MenuAccessController extends BaseController {

	@Autowired
	private MenuAccessService menuAccessService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private MenuService menuService;
	
	// url method menu
		@RequestMapping(value ="menuAccess")
		public String MenuAccess() {

			return "menuAccess";

		}
	
	@RequestMapping(value="menuAccess/add")
	public String add(Model model) throws Exception {
		
		//Skrip untuk combo box dinamis berdasarkan based on dta master
		List<RoleModel> roleModelList = null;
		roleModelList = this.roleService.list();
		model.addAttribute("roleModelList", roleModelList);
		
		List<MenuModel> menuModelList = null;
		menuModelList = this.menuService.list();
		model.addAttribute("menuModelList", menuModelList);
		// Akhir Skrip untuk combo box dinamis berdasarkan based on dta master
				
		return "menuAccess/add";
	}
	
	//url method set
			@RequestMapping(value="menuAccess/save")
			public String save(HttpServletRequest request, Model model) throws Exception {
				
				/*yg kiri variabel yg kanan dr jsp name nya*/
				Integer idMenuAccess = null;
				if (request.getParameter("idMenuAccess") != null) {
					idMenuAccess = Integer.valueOf(request.getParameter("idMenuAccess"));
				}
				
				Integer idRole = Integer.valueOf(request.getParameter("idRole"));
				Integer idMenu = Integer.valueOf(request.getParameter("idMenu"));
				
				MenuAccessModel menuAccessModel = new MenuAccessModel();
				
				menuAccessModel.setIdMenuAccess(idMenuAccess);
				menuAccessModel.setIdRole(idRole);
				menuAccessModel.setIdMenu(idMenu);
				
				String process = request.getParameter("process");
				
				if (process.equals("create")) {
					UserModel userModel = new UserModel();
					userModel = this.getUserModel();
					
					menuAccessModel.setCreatedByMenuAccess(userModel.getUsername());
					menuAccessModel.setCreatedDateMenuAccess(new Date());
					menuAccessModel.setIsDeleteMenuAccess(0);
					
					this.menuAccessService.create(menuAccessModel);
					
				}else if (process.equals("update")) {
					UserModel userModel = new UserModel();
					userModel = this.getUserModel();
					
					menuAccessModel.setUpdatedByMenuAccess(userModel.getUsername());
					menuAccessModel.setUpdatedDateMenuAccess(new Date());
					menuAccessModel.setIsDeleteMenuAccess(0);
					
					MenuAccessModel MaDB = new MenuAccessModel();
					MaDB = this.menuAccessService.searchById(idMenuAccess);
					
					menuAccessModel.setCreatedByMenuAccess(MaDB.getCreatedByMenuAccess());
					menuAccessModel.setCreatedDateMenuAccess(MaDB.getCreatedDateMenuAccess());
					
					this.menuAccessService.update(menuAccessModel);
					
				}else if (process.equals("delete")) {
					UserModel userModel = new UserModel();
					userModel = this.getUserModel();
					
					menuAccessModel.setUpdatedByMenuAccess(userModel.getUsername());
					menuAccessModel.setUpdatedDateMenuAccess(new Date());
					menuAccessModel.setIsDeleteMenuAccess(1);
					
					MenuAccessModel MaDB = new MenuAccessModel();
					MaDB = this.menuAccessService.searchById(idMenuAccess);
					
					menuAccessModel.setCreatedByMenuAccess(MaDB.getCreatedByMenuAccess());
					menuAccessModel.setCreatedDateMenuAccess(MaDB.getCreatedDateMenuAccess());
					
					this.menuAccessService.update(menuAccessModel);
					
				}else {
					
				}
				
				//ini sintax untuk kirim variabel jsp
				model.addAttribute("process", process);
				//return memanggil file supplier.jsp
				return "menuAccess";
				
			}
			
			//url method list
			@RequestMapping(value="menuAccess/list")
			private String list(Model model) throws Exception {
				List<MenuAccessModel> menuAccessModelList = new ArrayList<MenuAccessModel>();
				menuAccessModelList = this.menuAccessService.list();
				model.addAttribute("menuAccessModelList", menuAccessModelList);
				
				//Skrip untuk combo box dinamis berdasarkan based on dta master
				List<RoleModel> roleModelList = null;
				roleModelList = this.roleService.list();
				model.addAttribute("roleModelList", roleModelList);
				
				List<MenuModel> menuModelList = null;
				menuModelList = this.menuService.list();
				model.addAttribute("menuModelList", menuModelList);
				// Akhir Skrip untuk combo box dinamis berdasarkan based on dta master
				
				return "menuAccess/list";
			}
			
			//url method edit
			@RequestMapping(value="menuAccess/edit")
			private String edit(Model model, HttpServletRequest request) throws Exception {
				Integer idMenuAccess = Integer.valueOf(request.getParameter("idMenuAccess"));
				
				MenuAccessModel menuAccessModel = new MenuAccessModel();
				menuAccessModel = this.menuAccessService.searchById(idMenuAccess);
				
				model.addAttribute("menuAccessModel", menuAccessModel);
				
				//Skrip untuk combo box dinamis berdasarkan based on dta master
				List<RoleModel> roleModelList = null;
				roleModelList = this.roleService.list();
				model.addAttribute("roleModelList", roleModelList);
		
				List<MenuModel> menuModelList = null;
				menuModelList = this.menuService.list();
				model.addAttribute("menuModelList", menuModelList);
				// Akhir Skrip untuk combo box dinamis berdasarkan based on dta master
				
				return "menuAccess/edit";
			}

			//url method delete
			@RequestMapping(value="menuAccess/delete")
			private String delete(Model model, HttpServletRequest request) throws Exception {
				Integer idMenuAccess = Integer.valueOf(request.getParameter("idMenuAccess"));
				
				MenuAccessModel menuAccessModel = new MenuAccessModel();
				menuAccessModel = this.menuAccessService.searchById(idMenuAccess);
				
				model.addAttribute("menuAccessModel", menuAccessModel);
				
				//Skrip untuk combo box dinamis berdasarkan based on dta master
				List<RoleModel> roleModelList = null;
				roleModelList = this.roleService.list();
				model.addAttribute("roleModelList", roleModelList);
		
				List<MenuModel> menuModelList = null;
				menuModelList = this.menuService.list();
				model.addAttribute("menuModelList", menuModelList);
				// Akhir Skrip untuk combo box dinamis berdasarkan based on dta master
				
				return "menuAccess/delete";
			}
			
			//url method detail
			@RequestMapping(value="menuAccess/detail")
			private String detail(Model model, HttpServletRequest request) throws Exception {
				Integer idMenuAccess = Integer.valueOf(request.getParameter("idMenuAccess"));
				
				MenuAccessModel menuAccessModel = new MenuAccessModel();
				menuAccessModel = this.menuAccessService.searchById(idMenuAccess);
				
				model.addAttribute("menuAccessModel", menuAccessModel);
				
				//Skrip untuk combo box dinamis berdasarkan based on dta master
				List<RoleModel> roleModelList = null;
				roleModelList = this.roleService.list();
				model.addAttribute("roleModelList", roleModelList);
		
				List<MenuModel> menuModelList = null;
				menuModelList = this.menuService.list();
				model.addAttribute("menuModelList", menuModelList);
				// Akhir Skrip untuk combo box dinamis berdasarkan based on dta master
				
				return "menuAccess/detail";
			}


}
