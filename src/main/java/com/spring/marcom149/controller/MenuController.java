package com.spring.marcom149.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.marcom149.model.MenuModel;
import com.spring.marcom149.model.UserModel;
import com.spring.marcom149.service.MenuService;
import com.spring.marcom149.tools.KodeGenerator;

@Controller
public class MenuController extends BaseController {

	@Autowired
	private MenuService menuService;

	// url method menu
	@RequestMapping(value ="menu")
	public String Menu() {

		return "menu";

	}

	// url method halaman pop up add.jsp /tambah supplier
	@RequestMapping(value = "menu/add")
	public String add(Model model) throws Exception {
		// skrip ukt generate kodegenerator otomatis
		String kodeMenuGenerator = "";
		Boolean cek = false;
		while (cek == false) {
			MenuModel menuModel = new MenuModel();
			// kode digenerate dgn inisial namanya, semisal SUP
			kodeMenuGenerator = KodeGenerator.generator("ME");

			// Setelah itu dicek dulu apakah kode yg digenerate by sistem sudah ada di DB?
			menuModel = this.menuService.searchByCode(kodeMenuGenerator);

			// jika sudah ada, maka akan digenerate ulang
			if (menuModel == null) {
				cek = true;
			}

			model.addAttribute("kodeMenuGenerator", kodeMenuGenerator);

		}
		// akhir skrip utk generate kodegenerator otomatis
		// return memanggil file add di folder supplier
		return "menu/add";

	}

	// url method set
	@RequestMapping(value = "menu/save")
	public String save(HttpServletRequest request, Model model) throws Exception {

		/* yg kiri variabel yg kanan dr jsp name nya */
		Integer idMenu = null;
		if (request.getParameter("idMenu") != null) {
			idMenu = Integer.valueOf(request.getParameter("idMenu"));
		}
		String codeMenu = request.getParameter("codeMenu");
		String nameMenu = request.getParameter("nameMenu");
		String controllerMenu = request.getParameter("controllerMenu");

		MenuModel menuModel = new MenuModel();

		menuModel.setIdMenu(idMenu);
		menuModel.setCodeMenu(codeMenu);
		menuModel.setNameMenu(nameMenu);
		menuModel.setControllerMenu(controllerMenu);

		String process = request.getParameter("process");

		if (process.equals("create")) {
			
			UserModel userModel = new UserModel(); userModel = this.getUserModel();
			
			menuModel.setCreatedByMenu(userModel.getUsername());
			menuModel.setCreatedDateMenu(new Date());
			menuModel.setIsDeleteMenu(0);
			 

			this.menuService.create(menuModel);

		} else if (process.equals("update")) {
			
			UserModel userModel = new UserModel(); userModel = this.getUserModel();
			 
			menuModel.setUpdatedByMenu(userModel.getUsername());
			menuModel.setUpdatedDateMenu(new Date());
			menuModel.setIsDeleteMenu(0);
			 
			MenuModel MmDB = new MenuModel(); MmDB = this.menuService.searchById(idMenu);
			
			menuModel.setCreatedByMenu(MmDB.getCreatedByMenu());
			menuModel.setCreatedDateMenu(MmDB.getCreatedDateMenu());
			

			this.menuService.update(menuModel);

		} else if (process.equals("delete")) {
			
			 UserModel userModel = new UserModel(); userModel = this.getUserModel();
			 
			 menuModel.setUpdatedByMenu(userModel.getUsername());
			 menuModel.setUpdatedDateMenu(new Date());
			 menuModel.setIsDeleteMenu(1);
			 
			 MenuModel MmDB = new MenuModel(); MmDB = this.menuService.searchById(idMenu);
			 
			 menuModel.setCreatedByMenu(MmDB.getCreatedByMenu());
			 menuModel.setCreatedDateMenu(MmDB.getCreatedDateMenu());
			 

			/* this.menuService.delete(menuModel); */
			this.menuService.update(menuModel);

		} else {

		}

		// ini sintax untuk kirim variabel jsp
		model.addAttribute("process", process);
		model.addAttribute("menuModel", menuModel);

		// return memanggil file supplier.jsp
		return "menu";

	}

	// url method list
	@RequestMapping(value = "menu/list")
	private String list(Model model) throws Exception {
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
		menuModelList = this.menuService.list();
		model.addAttribute("menuModelList", menuModelList);

		return "menu/list";
	}
	
	@RequestMapping(value="menu/edit")
	private String edit(Model model, HttpServletRequest request) throws Exception {
	Integer idMenu = Integer.valueOf(request.getParameter("idMenu"));
		
	MenuModel menuModel = new MenuModel();
	menuModel = this.menuService.searchById(idMenu);
		
	model.addAttribute("menuModel", menuModel);
	return "menu/edit";
	}
	
	@RequestMapping(value="menu/delete")
	private String delete(Model model, HttpServletRequest request) throws Exception {
	Integer idMenu = Integer.valueOf(request.getParameter("idMenu"));
	
	MenuModel menuModel = new MenuModel();
	menuModel = this.menuService.searchById(idMenu);
		
	model.addAttribute("menuModel", menuModel);
	return "menu/delete";
	}
	
	@RequestMapping(value="menu/detail")
	private String detail(Model model, HttpServletRequest request) throws Exception {
	Integer idMenu = Integer.valueOf(request.getParameter("idMenu"));
	
	MenuModel menuModel = new MenuModel();
	menuModel = this.menuService.searchById(idMenu);
		
	model.addAttribute("menuModel", menuModel);
	return "menu/detail";
	}

}
