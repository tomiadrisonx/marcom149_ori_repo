package com.spring.marcom149.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.spring.marcom149.model.EventModel;
import com.spring.marcom149.model.MenuModel;
import com.spring.marcom149.model.UserModel;
import com.spring.marcom149.service.EventService;
import com.spring.marcom149.service.MenuService;
import com.spring.marcom149.tools.KodeGenerator;

@Controller
public class EventRequestController extends BaseController {

	@Autowired
	private EventService eventService;

	@Autowired
	private MenuService menuService;

	public void accesLogin(Model model) {
		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("nameRole", this.getUserModel().getRoleModel().getNameRole());

		// logic untuk list menu secara hirarki by role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().idRole);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
	}
	// akhir method untuk menampilakan menu by role login dan user login

	// url method event event
	@RequestMapping(value = "event_request")
	public String event(Model model) {
		this.accesLogin(model);
		return "event_request";

	}

	@RequestMapping(value = "event_request/add")
	public String add(Model model) throws Exception {

		// skrip untuk generate kodeGenerator
		String codeEventGenerator = "";
		Boolean cek = false;
		while (cek == false) {

			EventModel eventModel = new EventModel();
			// kode digenerate dengan inisial, semisal KAT
			codeEventGenerator = KodeGenerator.generator("TRWOEV");
			// Setelah itu dicek dulu apakah kode yang digenerate by sistem
			// sudah ada di db?
			eventModel = this.eventService.searchByKode(codeEventGenerator);

			// jika sudah ad, maka akan digenerate ulang
			if (eventModel == null) {
				cek = true;
			}

			model.addAttribute("codeEventGenerator", codeEventGenerator);

		}
		// akhir skrip untuk generate kodeGenerator

		UserModel userModel = new UserModel();
		userModel = this.getUserModel();
		Integer idUser = userModel.getIdUser();
		String username = userModel.getUsername();
		model.addAttribute("username", username);
		model.addAttribute("idUser", idUser);

		return "event_request/add";

	}

	// url method save
	@RequestMapping(value = "event_request/save")
	public String save(HttpServletRequest request, Model model) throws Exception {
		Integer idEvent = null;
		if (request.getParameter("idEvent") != null) {
			idEvent = Integer.valueOf(request.getParameter("idEvent"));
		}

		/* yang kiri adalah variable yang kanan dari jsp name nya */
		String codeEvent = request.getParameter("codeEvent");
		String nameEvent = request.getParameter("nameEvent");
		String placeEvent = request.getParameter("placeEvent");
		Integer requestByEvent = Integer.valueOf(request.getParameter("requestByEvent"));
		String noteEvent = request.getParameter("noteEvent");
		Long budgetEvent = Long.valueOf(request.getParameter("budgetEvent"));

		// code date
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
		Date startDateEvent = formatDate.parse(request.getParameter("startDateEvent"));
		Date endDateEvent = formatDate.parse(request.getParameter("endDateEvent"));
		Date requestDateEvent = formatDate.parse(request.getParameter("requestDateEvent"));

		EventModel eventModel = new EventModel();

		eventModel.setIdEvent(idEvent);
		eventModel.setCodeEvent(codeEvent);
		eventModel.setNameEvent(nameEvent);
		eventModel.setPlaceEvent(placeEvent);
		eventModel.setBudgetEvent(budgetEvent);
		eventModel.setRequestByEvent(requestByEvent);
		eventModel.setNoteEvent(noteEvent);
		eventModel.setStartDateEvent(startDateEvent);
		eventModel.setEndDateEvent(endDateEvent);
		eventModel.setRequestDateEvent(requestDateEvent);

		String process = request.getParameter("process");

		if (process.equals("create")) {

			UserModel user = new UserModel();
			user = this.getUserModel();
			eventModel.setCreatedByEvent(user.getUsername());
			eventModel.setCreatedDateEvent(new Date());
			eventModel.setIsDeleteEvent(0);

			eventModel.setStatusEvent(1); // status eventnya 1 karena submitted
			this.eventService.create(eventModel);

		} else if (process.equals("update")) {

			UserModel user = new UserModel();
			user = this.getUserModel();
			eventModel.setUpdatedByEvent(user.getUsername());
			eventModel.setUpdatedDateEvent(new Date());
			eventModel.setIsDeleteEvent(0);

			EventModel kmDB = new EventModel();
			kmDB = this.eventService.searchById(idEvent);

			eventModel.setCreatedByEvent(kmDB.getCreatedByEvent());
			eventModel.setCreatedDateEvent(kmDB.getCreatedDateEvent());

			this.eventService.update(eventModel);

		} else if (process.equals("delete")) {

			UserModel user = new UserModel();
			user = this.getUserModel();
			eventModel.setUpdatedByEvent(user.getUsername());
			eventModel.setUpdatedDateEvent(new Date());
			eventModel.setIsDeleteEvent(1);

			EventModel kmDB = new EventModel();
			kmDB = this.eventService.searchById(idEvent);

			eventModel.setCreatedByEvent(kmDB.getCreatedByEvent());
			eventModel.setCreatedDateEvent(kmDB.getCreatedDateEvent());

			this.eventService.update(eventModel);

		} else {

		}
		// ini syntax untuk kirim variable process ke jsp
		model.addAttribute("process", process);
		model.addAttribute("eventModel", eventModel);

		// return supplier maksudnya memanggil file supplier.jsp
		return "event_request";
	}

	// url method list
	@RequestMapping(value = "event_request/list")
	private String list(Model model) throws Exception {
		List<EventModel> eventModelList = new ArrayList<EventModel>();
		
		eventModelList = this.eventService.listByRequester(this.getUserModel().getIdUser());
		model.addAttribute("eventModelList", eventModelList);

		return "event_request/list";
	}

	// url method edit
	@RequestMapping(value = "event_request/edit")
	private String edit(Model model, HttpServletRequest request) throws Exception {
		Integer idEvent = Integer.valueOf(request.getParameter("idEvent"));

		EventModel eventModel = new EventModel();
		eventModel = this.eventService.searchById(idEvent);

		model.addAttribute("eventModel", eventModel);
		return "event_request/edit";

	}

	// url method delete
	@RequestMapping(value = "event_request/delete")
	private String delete(Model model, HttpServletRequest request) throws Exception {
		Integer idEvent = Integer.valueOf(request.getParameter("idEvent"));

		EventModel eventModel = new EventModel();
		eventModel = this.eventService.searchById(idEvent);

		model.addAttribute("eventModel", eventModel);
		return "event_request/delete";

	}

	// url method detail
	@RequestMapping(value = "event_request/detail")
	private String detail(Model model, HttpServletRequest request) throws Exception {
		Integer idEvent = Integer.valueOf(request.getParameter("idEvent"));

		EventModel eventModel = new EventModel();
		eventModel = this.eventService.searchById(idEvent);

		model.addAttribute("eventModel", eventModel);
		return "event_request/detail";

	}

}
