package com.spring.marcom149.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.marcom149.dao.EmployeeDao;
import com.spring.marcom149.model.EmployeeModel;
import com.spring.marcom149.service.EmployeeService;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService{
	
	@Autowired
	private EmployeeDao employeeDao;

	@Override
	public void create(EmployeeModel employeeModel) throws Exception {
		// TODO Auto-generated method stub
		this.employeeDao.create(employeeModel);
	}

	@Override
	public void update(EmployeeModel employeeModel) throws Exception {
		// TODO Auto-generated method stub
		this.employeeDao.update(employeeModel);
	}

	@Override
	public void delete(EmployeeModel employeeModel) throws Exception {
		// TODO Auto-generated method stub
		this.employeeDao.delete(employeeModel);
	}

	@Override
	public List<EmployeeModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.employeeDao.list();
	}

	@Override
	public EmployeeModel searchByKode(String codeEmployee) throws Exception {
		// TODO Auto-generated method stub
		return this.employeeDao.searchByKode(codeEmployee);
	}

	@Override
	public EmployeeModel searchById(Integer idEmployee) throws Exception {
		// TODO Auto-generated method stub
		return this.employeeDao.searchById(idEmployee);
	}

	@Override
	public List<EmployeeModel> listByRoleStaff() throws Exception {
		// TODO Auto-generated method stub
		return this.employeeDao.listByRoleStaff();
	}

}
