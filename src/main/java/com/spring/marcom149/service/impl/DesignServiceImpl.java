package com.spring.marcom149.service.impl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.marcom149.dao.DesignDao;
import com.spring.marcom149.model.DesignModel;
import com.spring.marcom149.service.DesignService;

@Service
@Transactional
public class DesignServiceImpl implements DesignService{
	
	@Autowired
	private DesignDao designDao;

	@Override
	public void create(DesignModel designModel) throws Exception {
		// TODO Auto-generated method stub
		this.designDao.create(designModel);
		
	}

	@Override
	public void update(DesignModel designModel) throws Exception {
		// TODO Auto-generated method stub
		this.designDao.update(designModel);
		
	}

	@Override
	public void delete(DesignModel designModel) throws Exception {
		// TODO Auto-generated method stub
		this.designDao.delete(designModel);
		
	}

	@Override
	public List<DesignModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.designDao.list();
	}		

	@Override
	public DesignModel searchByCode(String codeDesign) throws Exception {
		// TODO Auto-generated method stub
		return this.designDao.searchByCode(codeDesign);
	}
	
	@Override
	public DesignModel searchById(Integer idDesign) throws Exception {
		// TODO Auto-generated method stub
		return this.designDao.searchById(idDesign);
	}

}
