package com.spring.marcom149.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.marcom149.dao.EventDao;
import com.spring.marcom149.model.EventModel;
import com.spring.marcom149.service.EventService;

@Service
@Transactional
public class EventServiceImpl implements EventService{
	
	@Autowired
	private EventDao eventDao;
	
	@Override
	public void create(EventModel eventModel) throws Exception {
		// TODO Auto-generated method stub
		this.eventDao.create(eventModel);
	}
	
	@Override
	public void update(EventModel eventModel) throws Exception {
		// TODO Auto-generated method stub
		this.eventDao.update(eventModel);
	}

	@Override
	public void delete(EventModel eventModel) throws Exception {
		// TODO Auto-generated method stub
		this.eventDao.delete(eventModel);
	}

	@Override
	public List<EventModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.eventDao.list();
	}

	@Override
	public EventModel searchByKode(String codeEvent) throws Exception {
		// TODO Auto-generated method stub
		return this.eventDao.searchByKode(codeEvent);
	}

	@Override
	public EventModel searchById(Integer idEvent) throws Exception {
		// TODO Auto-generated method stub
		return this.eventDao.searchById(idEvent);
	}

	@Override
	public List<EventModel> listByRequester(Integer idUser) throws Exception {
		// TODO Auto-generated method stub
		return this.eventDao.listByRequester(idUser);
	}

	@Override
	public List<EventModel> listByAssignTo(Integer idUser) throws Exception {
		// TODO Auto-generated method stub
		return this.eventDao.listByAssignTo(idUser);
	}

	@Override
	public List<EventModel> listByApprove() throws Exception {
		// TODO Auto-generated method stub
		return this.eventDao.listByApprove();
	}

	
}
