package com.spring.marcom149.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.marcom149.dao.DesignItemDao;
import com.spring.marcom149.model.DesignItemModel;
import com.spring.marcom149.service.DesignItemService;

@Service
@Transactional
public class DesignItemServiceImpl implements DesignItemService {

	@Autowired
	private DesignItemDao designItemDao;
	
	@Override
	public void create(DesignItemModel designItemModel) throws Exception {
		// TODO Auto-generated method stub
		this.designItemDao.create(designItemModel);
	}

	@Override
	public void update(DesignItemModel designItemModel) throws Exception {
		// TODO Auto-generated method stub
		this.designItemDao.update(designItemModel);
	}

	@Override
	public void delete(DesignItemModel designItemModel) throws Exception {
		// TODO Auto-generated method stub
		this.designItemDao.delete(designItemModel);
	}

	@Override
	public DesignItemModel searchById(Integer idDesignItem) throws Exception {
		// TODO Auto-generated method stub
		return this.designItemDao.searchById(idDesignItem);
	}

	@Override
	public List<DesignItemModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.designItemDao.list();
	}

	
}
