package com.spring.marcom149.tools;

import java.util.UUID;

public class KodeGenerator {

	public static String generator(String jenisKode){
		String uuid = UUID.randomUUID().toString();
		String[]a = uuid.split("-");
		int count = a.length;
		String kode = jenisKode+a[count-1];
		return kode;
		
	}
}
