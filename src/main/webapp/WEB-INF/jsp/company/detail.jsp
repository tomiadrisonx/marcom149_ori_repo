
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-company">

	
<input type="hidden" id="process" name="process" value ="detail"/>
		<div class=form-horizontal>
			<h1>Detail Company</h1>
			<input type="hidden" id="idCompany" name="idCompany"
			value="${companyModel.idCompany}" />
			
			<div class="form-group">
			<label class="control-label col-md-3">Company Code</label>
			<div class="col-md-6">
			<input type="hidden" id="codeCompany" name="codeCompany" value="${companyModel.codeCompany}"/>
				<input type="text" class="form-input" id="codeCompanyDisplay"
					name="codeCompanyDisplay" oninput="setCustomValidity('')" disabled="disabled" value="${companyModel.codeCompany}"/>
			</div>

		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Name</label>
			<div class="col-md-6">
			<input type="hidden" id="nameCompany" name="nameCompany" value="${companyModel.nameCompany}"/>
				<input type="text" class="form-input" id="nameCompanyDisplay"
					name="nameCompanyDisplay" oninput="setCustomValidity('')" size="30" value="${companyModel.nameCompany}" disabled="disabled" />
			</div>

		</div>
		


			<div class="form-group">
				<label class="control-label col-md-3">Address</label>
				<div class="col-md-6">
				<input type="hidden" id="addressCompany" name="addressCompany" value="${companyModel.addressCompany}"/>
					<textarea rows="5" cols="20" class="form-input" id="addressCompany"
						name="addressCompany" oninput="setCustomValidity('')" disabled="disabled">${companyModel.addressCompany}</textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3">Phone</label>
				<div class="col-md-6">
				<input type="hidden" id="phoneCompany" name="phoneCompany" value="${companyModel.phoneCompany}"/>
					<input type="text" class="form-input" id="phoneCompanyDisplay"
						name="phoneCompanyDisplay" oninput="setCustomValidity('')" value="${companyModel.phoneCompany}" onkeypress="return validasiAngka(this)" disabled="disabled" />
				</div>

			</div>


			<div class="form-group">
				<label class="control-label col-md-3">Email</label>
				<div class="col-md-6">
				<input type="hidden" id="emailCompany" name="emailCompany" value="${companyModel.emailCompany}"/>
					<input type="text" class="form-input" id="emailCompanyDisplay"
						name="emailCompanyDisplay" oninput="setCustomValidity('')" value="${companyModel.emailCompany}" size="30" disabled="disabled" />
				</div>

			</div>
		
	</div>


</form>

<script type="text/javascript">

	function validasiAngka(evt) {
		var charAngka = (evt.which) ? evt.which : event.keyCode
		if ((charAngka > 31) && ((charAngka < 48) || (charAngka > 57))) {
			return false;
		} else {
			return true;
		}
	}

	function validasiInput() {
		var codeCompany = document.getElementById("codeCompany");
		var nameCompany = document.getElementById("nameCompany");
		
		if (codeCompany.value == "") {
			codeCompany.setCustomValidity("Kode Company Tidak Boleh Kosong");
		}else if (nameCompany.value == "") {
			nameCompany.setCustomValidity("Nama Company Tidak Boleh Kosong");
		}else {
			
		}
	}

</script>