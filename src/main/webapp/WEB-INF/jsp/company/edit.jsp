
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-company">

	
<input type="hidden" id="process" name="process" value ="update"/>
		<div class=form-horizontal>
			<h1>Update Company</h1>
			<input type="hidden" id="idCompany" name="idCompany"
			value="${companyModel.idCompany}" />
			
			<div class="form-group">
			<label class="control-label col-md-3">Company Code</label>
			<div class="col-md-6">
			<input type="hidden" id="codeCompany" name="codeCompany" value="${companyModel.codeCompany}"/>
				<input type="text" class="form-input" id="codeCompanyDisplay"
					name="codeCompanyDisplay" oninput="setCustomValidity('')" disabled="disabled" value="${companyModel.codeCompany}"/>
			</div>

		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Company Name</label>
			<div class="col-md-6">
				<input type="text" class="form-input" id="nameCompany"
					name="nameCompany" oninput="setCustomValidity('')" size="30" value="${companyModel.nameCompany}"/>
			</div>

		</div>
		


			<div class="form-group">
				<label class="control-label col-md-3">Company Address</label>
				<div class="col-md-6">
					<textarea rows="5" cols="20" class="form-input" id="addressCompany"
						name="addressCompany" oninput="setCustomValidity('')">${companyModel.addressCompany}</textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3">Company Phone</label>
				<div class="col-md-6">
					<input type="text" class="form-input" id="phoneCompany"
						name="phoneCompany" oninput="setCustomValidity('')" value="${companyModel.phoneCompany}" onkeypress="return validasiAngka(this)"/>
				</div>

			</div>


			<div class="form-group">
				<label class="control-label col-md-3">Company Email</label>
				<div class="col-md-6">
					<input type="text" class="form-input" id="emailCompany"
						name="emailCompany" oninput="setCustomValidity('')" size="30" value="${companyModel.emailCompany}"/>
				</div>

			</div>


			<div class="modal-footer">
				<button type="submit" class="btn btn-success" id="btn-save" onclick="validasiInput();">Update</button>

			</div>
		
	</div>


</form>

<script type="text/javascript">

	function validasiAngka(evt) {
		var charAngka = (evt.which) ? evt.which : event.keyCode
		if ((charAngka > 31) && ((charAngka < 48) || (charAngka > 57))) {
			return false;
		} else {
			return true;
		}
	}

	function validasiInput() {
		var codeCompany = document.getElementById("codeCompany");
		var nameCompany = document.getElementById("nameCompany");
		
		if (codeCompany.value == "") {
			codeCompany.setCustomValidity("Company Code Is Empty");
		}else if (nameCompany.value == "") {
			nameCompany.setCustomValidity("Company Name Is Empty");
		}else {
			
		}
	}

</script>