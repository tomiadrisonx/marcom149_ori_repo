
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-employee">

	
<input type="hidden" id="process" name="process" value ="delete"/>
		<div class=form-horizontal>
			<h1>Delete Employee</h1>
			<input type="hidden" id="idEmployee" name="idEmployee"
			value="${employeeModel.idEmployee}" />
			
			<div class="form-group">
			<label class="control-label col-md-3">Employee Code</label>
			<div class="col-md-6">
			<input type="hidden" id="codeEmployee" name="codeEmployee" value="${employeeModel.codeEmployee}"/>
				<input type="text" class="form-input" id="codeCompanyDisplay"
					name="codeCompanyDisplay" oninput="setCustomValidity('')" disabled="disabled" value="${employeeModel.codeEmployee}"/>
			</div>

		</div>

		<div class="form-group">
			<label class="control-label col-md-3">First Name</label>
			<div class="col-md-6">
			<input type="hidden" id="firstnameEmployee" name="firstnameEmployee" value="${employeeModel.firstnameEmployee}"/>
				<input type="text" class="form-input" id="firstnameEmployeeDisplay"
					name="firstnameEmployeeDisplay" size="30" oninput="setCustomValidity('')" value="${employeeModel.firstnameEmployee}" disabled="disabled" />
			</div>

		</div>
		


			<div class="form-group">
				<label class="control-label col-md-3">Last Name</label>
				<div class="col-md-6">
				<input type="hidden" id="lastnameEmployee" name="lastnameEmployee" value="${employeeModel.lastnameEmployee}"/>
					<input type="text" class="form-input" id="lastnameEmployeeDisplay"
						name="lastnameEmployeeDisplay" oninput="setCustomValidity('')" size="30" value="${employeeModel.lastnameEmployee}" disabled="disabled" />
				</div>
			</div>
			
			<div class="form-group">
			<label class="control-label col-md-3">Company</label>
			<div class="col-md-6">
			<input type="hidden" id="idCompany" name="idCompany" value="${employeeModel.idCompany}"/>
				<select class="form-input" id="idCompanyDisplay"
					name="idCompanyDisplay" oninput="setCustomValidity('')" disabled="disabled" >
					<c:forEach var="companyModel" items="${companyModelList}">
					<option value="${companyModel.idCompany}">${companyModel.nameCompany}</option>
					</c:forEach>
					</select>
			</div>
		</div>

			<div class="form-group">
				<label class="control-label col-md-3">Email</label>
				<div class="col-md-6">
				<input type="hidden" id="emailEmployee" name="emailEmployee" value="${employeeModel.emailEmployee}"/>
					<input type="text" class="form-input" id="emailEmployeeDisplay"
						name="emailEmployeeDisplay" oninput="setCustomValidity('')" size="30" value="${employeeModel.emailEmployee}" disabled="disabled" />
				</div>

			</div>


			<div class="modal-footer">
				<button type="submit" class="btn btn-danger" id="btn-save" onclick="validasiInput();">Delete</button>

			</div>
		
	</div>


</form>

<script type="text/javascript">

	function validasiAngka(evt) {
		var charAngka = (evt.which) ? evt.which : event.keyCode
		if ((charAngka > 31) && ((charAngka < 48) || (charAngka > 57))) {
			return false;
		} else {
			return true;
		}
	}

	function validasiInput() {
		var codeEmployee = document.getElementById("codeEmployee");
		var firstnameEmployee = document.getElementById("firstnameEmployee");
		
		if (codeEmployee.value == "") {
			codeEmployee.setCustomValidity("Employee Code Is Empty");
		}else if (firstnameEmployee.value == "") {
			firstnameEmployee.setCustomValidity("First Name Is Empty");
		}else {
			
		}
	}

</script>