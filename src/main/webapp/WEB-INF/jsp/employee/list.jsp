<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach var="employeeModel" items="${employeeModelList}">

	<tr>

		
		<td>${employeeModel.codeEmployee}</td>
		<td>${employeeModel.firstnameEmployee} ${employeeModel.lastnameEmployee}</td>
		<td>${employeeModel.companyModel.nameCompany}</td>
		<td>${employeeModel.createdByEmployee}</td>
		<td>${employeeModel.createdDateEmployee}</td>
		<td><button type="button" id="btn-edit" class="btn btn-success btn-xs btn-edit" value="${employeeModel.idEmployee}">Update</button>
		<button type="button" id="btn-delete" class="btn btn-danger btn-xs btn-delete" value="${employeeModel.idEmployee}">Delete</button>
		<button type="button" id="btn-detail" class="btn btn-info btn-xs btn-detail" value="${employeeModel.idEmployee}">Detail</button></td>

	</tr>

</c:forEach>