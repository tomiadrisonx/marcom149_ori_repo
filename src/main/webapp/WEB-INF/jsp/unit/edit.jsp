<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-unit">
<input type="hidden" id="process" name="process" value="update"/>

	<div class="form-horizontal">

		<div class="form-group">
			<h1>Halaman Edit Unit</h1>
			
			<input type="hidden" id="idUnit" name="idUnit" class="form-control"
			value="${unit.idUnit}">
			
	
			<label class="control-label col-md-3"> Unit Code </label>
			<div class="col-md-6">
			<input type="hidden" id="codeUnit" name="codeUnit" value ="${unit.codeUnit}"/>
				<input type="text" class="form-input" id="codeUnitDisplay"
					name="codeUnitDisplay" oninput="setCustomValidity('');" value ="${unit.codeUnit}" disabled="disabled"  />
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">*Unit Name</label>
			<div class="col-md-6">
				<input type="text" id="nameUnit" name="nameUnit" value ="${unit.nameUnit}"
					oninput="setCustomValidity('');" class="form-control"
					required="required">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Description Unit</label>
			<div class="col-md-6">
				<input type="text" id="descriptionUnit" name="descriptionUnit" value ="${unit.descriptionUnit}"
					oninput="setCustomValidity('');" class="form-control"
					required="required">
			</div>
		</div>

		

		<div class="modal-footer">
			<button type="submit" class="btn btn-success" id="btn_save"
				onclick="validasiInput();">Edit</button>
		</div>

	</div>

</form>
<script>

function validasiInput() {
	var codeUnit = document.getElementById("codeUnit")
	var nameUnit = document.getElementById("nameUnit")
		
	if (codeUnit.value=="") {
		codeUnit.setCustomValidity("Unit Code tidak boleh kosong");
		
	}else if (nameUnit.value=="") {
		nameUnit.setCustomValidity("Unit Name tidak boleh kosong");
	
	} else{
		
	}

	
}

</script>