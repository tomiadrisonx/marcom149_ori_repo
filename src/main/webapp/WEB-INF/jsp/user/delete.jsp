<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-user">
	<input type="hidden" id="process" name="process" value="delete" />
	<div class="form-horizontal">
		<h4>Edit User</h4>

		<input type="hidden" id="idUser" name="idUser"
			value="${userModel.idUser}" />

		<div class="form-group">
			<label class="control-label col-md-3">*Role Name</label>
			<div class="col-md-6">
				<input type="hidden" id="idRole" name="idRole"
					value="${userModel.idRole}" /> <select class="form-input"
					id="idRoleDisplay" name="idRoleDisplay" class="form-control"
					oninput="setCustomValidity('')" required
					oninvalid="this.setCustomValidity('Please Select Role Name')"
					disabled="disabled">
					<c:forEach var="roleModel" items="${roleModelList}">
						<option value="${roleModel.idRole}">
							${roleModel.nameRole}</option>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">*Employee Name</label>
			<div class="col-md-6">
				<input type="hidden" id="idEmployee" name="idEmployee"
					value="${userModel.idEmployee}" /> <select class="form-input"
					id="idEmployeeDisplay" name="idEmployeeDisplay"
					class="form-control" oninput="setCustomValidity('')" required
					oninvalid="this.setCustomValidity('Please Select Employee Name')"
					disabled="disabled">
					<option value="">-- Select Employee Name ---</option>
					<c:forEach var="employeeModel" items="${employeeModelList}">
						<option value="${employeeModel.idEmployee}">
							${employeeModel.firstnameEmployee} ${employeeModel.lastnameEmployee}</option>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">*Username</label>
			<div class="col-md-6">
				<input type="hidden" id="username" name="username"
					value="${userModel.username}" /> <input type="text"
					class="form-input " id="usernameDisplay" name="usernameDisplay"
					oninput="setCustomValidity('')" placeholder="Username"
					value="${userModel.username}" disabled="disabled" />
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">*Password</label>
			<div class="col-md-6">
				<input type="hidden" id="password" name="password"
					value="${userModel.password}" /> <input type="password"
					class="form-input " id="passwordDisplay" name="passwordDisplay"
					oninput="setCustomValidity('')" placeholder="Password"
					value="${userModel.password}" disabled="disabled" />
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">*Re-Type Password</label>
			<div class="col-md-6">
				<input type="hidden" id="retypePassword" name="retypePassword"
					value="${userModel.password}" /> <input type="password"
					class="form-input " id="retypePasswordDisplay"
					name="retypePasswordDisplay" oninput="setCustomValidity('')"
					placeholder="Re-Type Password" onchange="cekPassword();"
					value="${userModel.password}" disabled="disabled" />
			</div>
		</div>

		<div class="modal-footer">
			Delete Data?
			<button type="submit" class="btn btn-danger" id="btn-save">Delete</button>
		</div>

	</div>
</form>

<script type="text/javascript">
	function validasiInput() {

		var username = document.getElementById("username");
		var password = document.getElementById("password");
		var retypePassword = document.getElementById("retypePassword");

		if (username.value == '') {
			username
					.setCustomValidity('Username Empty! Please fill out this field');
		} else if (password.value == '') {
			password
					.setCustomValidity('Password Empty! Please fill out this field');
		} else if (retypePassword.value == '') {
			retypePassword
					.setCustomValidity('Retype Password Empty! Please fill out this field');
		} else {

		}
	}

	function cekPassword() {
		var password = document.getElementById('password');
		var retypePassword = document.getElementById('retypePassword');

		if (retypePassword.value != password.value) {
			retypePassword.setCustomValidity('Password Not Same');
		} else {

		}
	}
</script>
