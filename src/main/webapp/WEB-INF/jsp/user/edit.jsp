<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-user">
	<input type="hidden" id="process" name="process" value="update" />
	<div class="form-horizontal">
		<h4>Edit User - <%-- ${employeeModel.nameEmployee}  --%>(${userModel.username})</h4>
		
		<input type="hidden" id="idUser" name="idUser" value="${userModel.idUser}"/>

		<div class="form-group">
			<label class="control-label col-md-3">*Role Name</label>
			<div class="col-md-6">
				<select class="form-input" id="idRole" name="idRole" class="form-control" 
				oninput="setCustomValidity('')" required
					oninvalid="this.setCustomValidity('Please Select Role Name')">					
					<c:forEach var="roleModel" items="${roleModelList}">
						<option value="${roleModel.idRole}">
							${roleModel.nameRole}</option>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">*Employee Name</label>
			<div class="col-md-6">
				<select class="form-input" id="idEmployee" name="idEmployee"
					class="form-control" oninput="setCustomValidity('')" required
					oninvalid="this.setCustomValidity('Please Select Employee Name')">
					<option value="">-- Select Employee Name ---</option>
					<c:forEach var="employeeModel" items="${employeeModelList}">
						<option value="${employeeModel.idEmployee}">
							${employeeModel.firstnameEmployee} ${employeeModel.lastnameEmployee}</option>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">*Username</label>
			<div class="col-md-6">
				<input type="text" class="form-input " id="username" name="username"
					oninput="setCustomValidity('')" placeholder="Username" 
					required title="8 Character Username" 
					pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{7,20}$"
					value="${userModel.username}"/>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">*Password</label>
			<div class="col-md-6">
				<input type="password" class="form-input " id="password" name="password"
					oninput="setCustomValidity('')" required title = "8 Character Password,Use Uppercase, Lowercase, and Numeric"
					pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
					placeholder="Password" value="${userModel.password}"/>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">*Re-Type Password</label>
			<div class="col-md-6">
				<input type="password" class="form-input " id="retypePassword"
					name="retypePassword" oninput="setCustomValidity('')"
					placeholder="Re-Type Password" onchange="cekPassword();" value="${userModel.password}"/>
			</div>
		</div>

		<div class="modal-footer">
			<button type="submit" class="btn btn-success" id="btn-save"
				onclick="validasiInput();">Update</button>
		</div>

	</div>
</form>

<script type="text/javascript">
	function validasiInput() {

		var username = document.getElementById("username");
		var password = document.getElementById("password");
		var retypePassword = document.getElementById("retypePassword");

		if (username.value == '') {
			username.setCustomValidity('Username Empty! Please fill out this field');
		} else if (password.value == '') {
			password.setCustomValidity('Password Empty! Please fill out this field');
		} else if (retypePassword.value == '') {
			retypePassword.setCustomValidity('Retype Password Empty! Please fill out this field');
		} else {

		}
	}

	function cekPassword() {
		var password = document.getElementById('password');
		var retypePassword = document.getElementById('retypePassword');

		if (retypePassword.value != password.value) {
			retypePassword.setCustomValidity('Password Not Same');
		} else {

		}
	}
</script>
