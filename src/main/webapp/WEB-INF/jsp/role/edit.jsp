<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-role">
	<input type="hidden" id="process" name="process" value="update"/>
	<div class="form-horizontal">
	
		<h4>Edit Role - ${roleModel.nameRole} (${roleModel.codeRole})</h4>
		
		<input type="hidden" id="idRole" name="idRole" value="${roleModel.idRole}"/>
		<div class="form-group">
			<label class="control-label col-md-3">*Role Code</label>
			<div class="col-md-6">
				<input type="hidden" id="codeRole" name="codeRole" value="${roleModel.codeRole}"/>
				<input type="text" class="form-input " id="codeRoleDisplay"
					name="codeRoleDisplay" oninput="setCustomValidity('')" disabled="disabled" value="${roleModel.codeRole}"/>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">*Role Name</label>
			<div class="col-md-6">
				<input type="text" class="form-input " id="nameRole"
					name="nameRole" oninput="setCustomValidity('')" value="${roleModel.nameRole}"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Description</label>
			<div class="col-md-6">
				<textarea rows="5" cols="50" class="form-input" id="descriptionRole"
					name="descriptionRole">${roleModel.descriptionRole}</textarea>
			</div>
		</div>

		<div class="modal-footer">
			<button type="submit" class="btn btn-success" id="btn-save"
				onclick="validasiInput();">Save</button>
		</div>

	</div>
</form>

<script type="text/javascript">
	function validasiInput() {
		var nameRole = document.getElementById("nameRole");

		if (nameRole.value == '') {
			nameRole.setCustomValidity('Role Name Empty! Please fill out this field');
		} else {

		}

	}
</script>