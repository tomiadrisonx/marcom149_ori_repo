<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-menu">
	<input type="hidden" id="process" name="process" value="delete"/>
	<div class="form-horizontal">
		<h3>View Menu - ${menuModel.nameMenu} (${menuModel.codeMenu})</h3>
		<input type="hidden" id="idMenu" name="idMenu" value="${menuModel.idMenu}"/>
		<div class="form-group">
			<label Class="control-label col-md-6">Menu Code</label>
			<div class="col-md-6">
				<input type="hidden" id="codeMenu" name="codeMenu" value="${menuModel.codeMenu}"/>
				<input type="text" Class="form-input" id="codeMenuDisplay" 
				name="codeMenuDisplay" disabled="disabled" value="${menuModel.codeMenu}"/>
			</div>
		</div>
		
		<div class="form-group">
			<label Class="control-label col-md-6">Menu Name</label>
			<div class="col-md-6">
				<input type="hidden" id="nameMenu" name="nameMenu" value="${menuModel.nameMenu}"/>
				<input type="text" Class="form-input" 
				id="nameMenuDisplay" name="nameMenuDisplay" disabled="disabled" value="${menuModel.nameMenu}"/>
			</div>
		</div>
		
		<div class="form-group">
			<label Class="control-label col-md-6">Controller Menu</label>
			<div class="col-md-6">
				<input type="hidden" id="controllerMenu" name="controllerMenu" 
				value="${menuModel.controllerMenu}"/>
				<input type="text" Class="form-input" id="controllerMenuDisplay" 
				oninput="setCustomValidity('')" name="controllerMenuDisplay" 
				value="${menuModel.controllerMenu}" disabled="disabled"/>
			</div>
		</div>		
		
	</div>

</form>
