<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-menu">
	<input type="hidden" id="process" name="process" value="create"/>
	<div class="form-horizontal">
		<h3>Add Menu</h3>
		<div class="form-group">
			<label Class="control-label col-md-6">*Menu Code</label>
			<div class="col-md-6">
				<input type="hidden" id="codeMenu" name="codeMenu" value="${kodeMenuGenerator}"/>
				<input type="text" Class="form-input" id="codeMenuDisplay" name="codeMenuDisplay" disabled="disabled" value="${kodeMenuGenerator}"/>
			</div>
		</div>
		
		<div class="form-group">
			<label Class="control-label col-md-6">*Menu Name</label>
			<div class="col-md-6">
				<input type="text" id="nameMenu" name="nameMenu" />
			</div>
		</div>
		
		<div class="form-group">
			<label Class="control-label col-md-6">Controller Name</label>
			<div class="col-md-6">
				<input type="text" Class="form-input" id="controllerMenu" 
				oninput="setCustomValidity('')" name="controllerMenu"/>
			</div>
		</div>
		
				<div class="modal-footer">
			<button type="submit" onclick="validasiInput();"
			class="btn btn-success" id="btn-save">Simpan</button>
		</div>
		
	</div>

</form>

<script>
	
	function validasiInput() {
		var codeMenu = document.getElementById("codeMenu");
		var nameMenu = document.getElementById("nameMenu");
		var controllerMenu = document.getElementById("controllerMenu");
		
		if (codeMenu.value == "") {
			codeMenu.setCustomValidity("Code Menu Tidak Boleh Kosong");
		}else if (nameMenu.value == "") {
			nameMenu.setCustomValidity("Name Menu Tidak Boleh Kosong");
		}else if (controllerMenu.value == "") {
			controllerMenu.setCustomValidity("Controller Menu Tidak Boleh Kosong");
		}else {
			
		}
	}
	
</script>