<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-menu">
	<input type="hidden" id="process" name="process" value="update"/>
	<div class="form-horizontal">
		<h3>Edit Menu - ${menuModel.nameMenu} (${menuModel.codeMenu})</h3>
		<input type="hidden" id="idMenu" name="idMenu" value="${menuModel.idMenu}"/>
		<div class="form-group">
			<label Class="control-label col-md-6">Menu Code</label>
			<div class="col-md-6">
				<input type="hidden" id="codeMenu" name="codeMenu" value="${menuModel.codeMenu}"/>
				<input type="text" Class="form-input" id="codeMenuDisplay" name="codeMenuDisplay" 
				disabled="disabled" value="${menuModel.codeMenu}"/>
			</div>
		</div>
		
		<div class="form-group">
			<label Class="control-label col-md-6">Menu Name</label>
			<div class="col-md-6">
				<input type="text" id="nameMenu" name="nameMenu" value="${menuModel.nameMenu}"/>
			</div>
		</div>
		
		<div class="form-group">
			<label Class="control-label col-md-6">Controller Name</label>
			<div class="col-md-6">
				<input type="text" Class="form-input" id="controllerMenu" 
				oninput="setCustomValidity('')" name="controllerMenu" value="${menuModel.controllerMenu}"/>
			</div>
		</div>
		
				<div class="modal-footer">
			<button type="submit" onclick="validasiInput();"
			class="btn btn-success" id="btn-save">Simpan</button>
		</div>
		
	</div>

</form>

<script>
	
	function validasiInput() {
		var nameMenu = document.getElementById("nameMenu");
		var controllerName = document.getElementById("controllerName");
		
		if (nameMenu.value == "") {
			nameMenu.setCustomValidity("Name Menu Tidak Boleh Kosong");
		}else if (controllerName.value == "") {
			controllerName.setCustomValidity("Controller Name Tidak Boleh Kosong");
		}else {
			
		}
	}
	
</script>