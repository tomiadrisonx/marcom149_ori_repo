<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach var="menuModel" items="${menuModelList }">

	<tr>
		<td>${menuModel.codeMenu}</td>		
		<td>${menuModel.nameMenu}</td>
		<td>${menuModel.createdDateMenu}</td>
		<td>${menuModel.createdByMenu}</td>	
		<td>
			<button type="button" id="btn-edit" 
			class="btn btn-success btn-xs btn-edit" value="${menuModel.idMenu}">Edit</button>
			<button type="button" id="btn-delete" 
			class="btn btn-danger btn-xs btn-delete" value="${menuModel.idMenu}">Delete</button>	
			<button type="button" id="btn-detail" 
			class="btn btn-info btn-xs btn-detail" value="${menuModel.idMenu}">Detail</button>
		</td>
	</tr>
	
</c:forEach>