<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach var="menuAccessModel" items="${menuAccessModelList }">

	<tr>
		<td>${menuAccessModel.roleModel.codeRole}</td>		
		<td>${menuAccessModel.roleModel.nameRole}</td>		
		<td>${menuAccessModel.menuModel.nameMenu}</td>		
		<td>${menuAccessModel.createdDateMenuAccess}</td>		
		<td>${menuAccessModel.createdByMenuAccess}</td>		
		<td>
			<button type="button" id="btn-edit" 
			class="btn btn-success btn-xs btn-edit" value="${menuAccessModel.idMenuAccess}">Edit</button>
			<button type="button" id="btn-delete" 
			class="btn btn-danger btn-xs btn-delete" value="${menuAccessModel.idMenuAccess}">Delete</button>	
		</td>
	</tr>
	
</c:forEach>