<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-menuAccess">
	<input type="hidden" id="process" name="process" value="create"/>
	<div class="form-horizontal">
		<h3>Add Menu Access</h3>
		<div class="form-group">
			<label Class="control-label col-md-6">*Role Code</label>
			<div class="col-md-6">
				<select id="idRole" name="idRole" oninput="setCustomValidity('')" class="form-input">
					<c:forEach var="roleModel" items="${roleModelList}">
						<option value="${roleModel.idRole}">${roleModel.nameRole}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label Class="control-label col-md-6">*Menu Access</label>
			<div class="col-md-6">
				<select id="idMenu" name="idMenu" oninput="setCustomValidity('')" class="form-input">
					<c:forEach var="menuModel" items="${menuModelList}">
						<option value="${menuModel.idMenu}">${menuModel.controllerMenu}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		<div class="modal-footer">
			<button type="submit" onclick="validasiInput();"
			class="btn btn-success" id="btn-save">Save</button>
		</div>
		
	</div>

</form>
