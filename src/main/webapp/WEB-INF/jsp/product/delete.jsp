<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-product">
<input type="hidden" id="process" name="process" value="delete"/>

	<div class="form-horizontal">

		<div class="form-group">
			<h1>Halaman Edit Product</h1>
			
			<input type="hidden" id="idProduct" name="idProduct" class="form-control"
			value="${product.idProduct}">
			
	
			<label class="control-label col-md-3"> Product Code </label>
			<div class="col-md-6">
			<input type="hidden" id="codeProduct" name="codeProduct" value ="${product.codeProduct}"/>
				<input type="text" class="form-input" id="codeProductDisplay"
					name="codeProductDisplay" oninput="setCustomValidity('');" value ="${product.codeProduct}" disabled="disabled"  />
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">*Product Name</label>
			<div class="col-md-6">
			<input type="hidden" id="nameProduct" name="nameProduct" value ="${product.nameProduct}"/>
				<input type="text" id="nameProductDisplay" name="nameProductDisplay" value ="${product.nameProduct}" disabled="disabled"
					oninput="setCustomValidity('');" class="form-control"
					required="required">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Description Product</label>
			<div class="col-md-6">
			<input type="hidden" id="descriptionProduct" name="descriptionProduct" value ="${product.descriptionProduct}"/>
				<input type="text" id="descriptionProductDisplay" name="descriptionProductDisplay" value ="${product.descriptionProduct}" disabled="disabled"
					oninput="setCustomValidity('');" class="form-control"
					required="required">
			</div>
		</div>

		

		<div class="modal-footer">
			<button type="submit" class="btn btn-success" id="btn_save"
				onclick="validasiInput();">Delete</button>
		</div>

	</div>

</form>
<script>

function validasiInput() {
	var codeProduct = document.getElementById("codeProduct")
	var nameProduct = document.getElementById("nameProduct")
		
	if (codeProduct.value=="") {
		codeProduct.setCustomValidity("Product Code tidak boleh kosong");
		
	}else if (nameProduct.value=="") {
		nameProduct.setCustomValidity("Product Name tidak boleh kosong");
	
	} else{
		
	}

	
}

</script>